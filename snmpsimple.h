#ifndef SNMPSIMPLE_H
#define SNMPSIMPLE_H

#include <QObject>
#include <snmp_pp/snmp_pp.h>
#include <QTimer>

#include <QDebug>

//#define TIMEOUT 1000

using namespace Snmp_pp;

class SnmpSimple: public QObject
{
    Q_OBJECT

    QTimer *getTime;
    /*параметры snmp*/

    Snmp *snmp;
    SnmpTarget *target;
    CTarget ctarget;
    unsigned int timeout;
    bool initializeTarget;

protected:
    UdpAddress address;
    unsigned short port;
    int interval;
    QByteArray readCommunityName,writeCommunityName;
    bool connectStat;


public:    
    explicit SnmpSimple(QObject *parent = 0);
    bool setSnmp(const QString& oidstr,const int &value);    
    QStringList getSnmp(QStringList oidstr);
    QString getSnmp(const QString& oidstr);

    bool initializeSnmp();

    ~SnmpSimple();

signals:
    void error(QString error);
    void disconnected();
    void connected();
    void startTimer();
    void stopTimer();

public slots:
    void start();
    //virtual void connectToSnmp(const QString &ipaddr){}
    virtual void getStat(){}
    void disconnectFromSnmp();
    bool isConnected(){qDebug() << "conStat" << connectStat; return connectStat;}

};

#endif // SNMPSIMPLE_H
