#ifndef TEST_H
#define TEST_H

#include <QObject>
#include <QSignalMapper>
#include "RRC/radiorelaynew.h"
#include "dlink/dlink3120new.h"
#include "channelprocessing.h"

class Test : public QObject
{
    Q_OBJECT
    QVector <RadioRelay *> radioRelay[2];
    QVector <bool> radioRelayConnectStatus[2];
    QVector <Dlink3120new * > dlink3120;
    QVector <ChannelProcessing *> channelProcessing;
    QVector <bool> channelIsActive;
    bool testIsActive;
    QTimer *onePassTimer;//время одного прохода
    QVector <QTimer * > fullPassTimer; //массив таймеров длительности всего теста
    QTimer *updateTimer;//таймер, который раз в секунду обновляет информацию в виджете о оставшемся времени теста
    QTime timeElapsed;//время прошедшее с начала прохода
    QString trafficGeneratorPort;
    QSignalMapper *disconnectChannelMapper[2];

    const long int oneDay;

    void checkChannelIsActive();
    void createConnect();
    void updateTimerLabel();
    void onePassTimeOut();
    void dlinkSeparate(QVector<DlinkInfo> dlinkInfo);

public:
    explicit Test(QVector<Dlink3120new *> &dlink3120, QVector<RadioRelay *> (&radioRelay)[2],QVector <ChannelProcessing *> &channelProcessing, Config &cnf, QObject *parent = 0);
    void startTest();
    void setOnePassInterval(const int& msec);
    void setFullPassInterval(const int& msec);

    ~Test();

signals:
    void updateOnePassTimerLabel(const QString& str);
    void updateFullPassTimerLabel(QStringList str);
    void turnOffDlink(const QString &port);//выключает порт на длинке
    void clearALLstat();//очищает статистику на длинке
    void turnOnDlink(const QString &port);//включает порт на длинке
    void separateDlinkInfoSignal(DlinkInfo dlinkInfo);//посылает инфу о длинке  в класс обработки информации
    void getStatDlink();//запрашивает информацию у длинка по всем портам
    void channelActive(const int numChannel, bool state);//включение или отключение виджетов во время теста
    void testActive(const bool& state);
    void clearData();
    void escalateAttValue();

    void toLog(QString str);

public slots:
    void stopChannel(const int &numChannel);
    void disconnectFromRRC(const int &numChannel);
    //void slot();

};

#endif // TEST_H
