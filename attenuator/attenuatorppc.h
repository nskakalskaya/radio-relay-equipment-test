#ifndef ATTENUATORPPC_H
#define ATTENUATORPPC_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTimer>
#include <QDebug>
#include <windows.h>
#include <QString>
#include <QByteArray>
#include <QMap>
#include <QFile>
#include "SLABCP2110.h"

#define TIMEOUT 100

class AttenuatorPPC : public QObject
{
    Q_OBJECT
    static const WORD VID = 0x10C4;
    static const WORD PID = 0xEA80;
    static const DWORD baudRate = 115200;
    static const BYTE dataBits = HID_UART_EIGHT_DATA_BITS;
    static const BYTE parity =  HID_UART_NO_PARITY;
    static const BYTE stopBits = HID_UART_SHORT_STOP_BIT;
    static const BYTE flowControl = HID_UART_NO_FLOW_CONTROL;
    QMap<int, QString> fixedMap;
    enum class Direction {
        up, down
    };
    Direction dir;
    BYTE lastByte1, lastByte2, maxByte1, maxByte2, minByte1, minByte2, devAddrByte;
    HID_UART_DEVICE devHandle;
    QMap<int, QString> devices;
    QString initInformation, attenuatorName;
    int minValue, maxValue, curValue, codeFreqDiap, responseCode;
    bool connectionState;
    void raiseAtt();
    void reduceAtt();
    void writeCommand(BYTE devAddr, BYTE channelNum);
    /*QSerialPort serialPort;
    QString name;
    qint32 baudRate;
    QSerialPort::DataBits dataBits;
    QSerialPort::Parity parity;
    QSerialPort::StopBits stopBits;
    QSerialPort::FlowControl flowControl;
    void write(const QByteArray &send);
    bool writeSTA();
    bool readATN();
    bool VeryfiCRC(const QByteArray &data,QStringList &dataSplit);
    QByteArray generateSendCommand(const QString &command,const QString &commandCheckSumm,const QString &hexATT);*/

public:
    explicit AttenuatorPPC(QObject *parent = 0);
    bool isConnected() { return connectionState; }
    int getCurValue() { return curValue; }
    int getMinValue() { return minValue; }
    int getMaxValue() { return maxValue; }
    void disconnectFromAttenuator();
    ~AttenuatorPPC();

signals:
    void haveID(const QString& str);
    void haveMinDb(QString str);
    void haveMaxDb(QString str);
    void errorMSG(QString str);
    void disconnected();
    void connected();

public slots:
   // void writeATT(const unsigned short& ATT);
    void connectToAttenuator(int devicesIndex);
    void escalateValue();
    void startEscalating();
    void stopEscalating();
    void getMinDb(int min);
    void getMaxDb(int max);
};

#endif // ATTENUATORPPC_H
