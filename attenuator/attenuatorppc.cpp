#include "attenuatorppc.h"

AttenuatorPPC::AttenuatorPPC(QObject *parent) :
    QObject(parent), 
    dir(Direction::up),
    minValue(0),
    maxValue(0),
    curValue(0),
    codeFreqDiap(0),
    responseCode(0),
    connectionState(false)
{
    DWORD numOfDevices = 0;
    HID_UART_DEVICE_STR serialStr;
    HidUart_GetNumDevices(&numOfDevices, VID, PID);
    for (int i = 0; i < numOfDevices; i++) {
        HidUart_GetString(i, VID, PID, serialStr, HID_UART_GET_SERIAL_STR);
        fixedMap.insert(i, serialStr);
    }
}

void AttenuatorPPC::connectToAttenuator(int devicesIndex)
{
    HID_UART_STATUS	status = HID_UART_DEVICE_NOT_FOUND;
    devices.clear();
    QFile t("dev.txt");
    t.open(QIODevice::Append);
    QTextStream st(&t);
    DWORD numOfDevices = 0;
    HidUart_GetNumDevices(&numOfDevices, VID, PID);
    st << QString::number(numOfDevices);
  //  for (int i = 0; i < numOfDevices; i++) {
      //  HID_UART_DEVICE_STR serialStr;
//        if (HidUart_GetString(i, VID, PID, serialStr, HID_UART_GET_SERIAL_STR) == HID_UART_SUCCESS) {
//             st << serialStr << endl;
//             if (serialStr == fixedMap.value(devicesIndex)) {
//                st << serialStr << endl;
                status = HidUart_Open(&devHandle, devicesIndex, VID, PID);
                if (status == HID_UART_SUCCESS) {
                    st << "opened" << endl;
                    // break;
                }
          //  }
       // }
 //   }
    if (status == HID_UART_SUCCESS)
        status = HidUart_SetUartConfig(devHandle, baudRate, dataBits, parity, stopBits, flowControl);
    if (status == HID_UART_SUCCESS) {
        DWORD vBaudRate;
        BYTE vDataBits, vParity, vStopBits, vFlowControl;
        status = HidUart_GetUartConfig(devHandle, &vBaudRate, &vDataBits, &vParity, &vStopBits, &vFlowControl);
        if (status == HID_UART_SUCCESS)
            if (vBaudRate != baudRate || vDataBits != dataBits || vParity != parity || vStopBits != stopBits || vFlowControl != flowControl)
                status = HID_UART_INVALID_PARAMETER;
   }
   if (status == HID_UART_SUCCESS)
       status = HidUart_SetTimeouts(devHandle, 0, 2000);
   if (status == HID_UART_SUCCESS) {
        st << "connected" << endl;
        emit connected();
        connectionState = true;
   }
   t.close();
}

void AttenuatorPPC::escalateValue()
{
    if ((lastByte1 == maxByte1) && (lastByte2 == maxByte2) && (dir == Direction::up))  {
            reduceAtt();
            dir = Direction::down;
    }
    else if ((lastByte1 == minByte1) && (lastByte2 == minByte2) && (dir == Direction::down)) {
        raiseAtt();
        dir = Direction::up;
    }
    else if (dir == Direction::up)
            raiseAtt();
    else
        reduceAtt();
    writeCommand(devAddrByte, 0x01);
    Sleep(500);
    writeCommand(devAddrByte, 0x02);
    Sleep(500);
}

void AttenuatorPPC::startEscalating()
{
    DWORD numBytesWritten = 0;
    BYTE buffer[6]	 = {0x3a, 0x00, 0x57, 0x48, 0x4f, 0x23};
    DWORD numBytesToWrite = sizeof(buffer);
    HidUart_Write(devHandle, buffer, numBytesToWrite, &numBytesWritten);
    HID_UART_STATUS status;
    DWORD numBytesRead	= 0;
    DWORD numBytesToRead = 1000;
    BYTE* received	= new BYTE[numBytesToRead];
    Sleep(500);
    status = HidUart_Read(devHandle, received, numBytesToRead, &numBytesRead);
    devAddrByte = received[0];
    Sleep(500);
    writeCommand(devAddrByte, 0x01);
    Sleep(500);
    writeCommand(devAddrByte, 0x02);
    Sleep(500);
}

void AttenuatorPPC::stopEscalating()
{

}

void AttenuatorPPC::reduceAtt()
{
   if ((lastByte2 & 0x0F) == 0) {
       lastByte1 -= 0x01;
       lastByte2 += 0x05;
   }
   else if ((lastByte2 & 0x0F) == 5)
       lastByte2 -= 0x05;
}

void AttenuatorPPC::writeCommand(BYTE devAddr, BYTE channelNum)
{
    BYTE channel[10] = {0x3a, 0x05, 0x41, 0x54, 0x54, devAddr, channelNum, lastByte1, lastByte2, 0x23};
    DWORD numBytesToWrite = sizeof(channel);
    DWORD numBytesWritten = 0;
    HidUart_Write(devHandle, channel, numBytesToWrite, &numBytesWritten);
}

void AttenuatorPPC::raiseAtt()
{
    if ((lastByte2 & 0x0F) == 0)
        lastByte2 += 0x05;
    else if ((lastByte2 & 0x0F) == 5) {
        lastByte2 = 0x30;
        lastByte1 += 0x01;
     }
}

void AttenuatorPPC::getMinDb(int min)
{
    minValue = min;
    lastByte1 = lastByte2 = 0x30;
    min = min / 10 % 10;
    lastByte1 += min;
    min = minValue % 10;
    lastByte2 += min;
    minByte1 = lastByte1;
    minByte2 = lastByte2;
}

void AttenuatorPPC::getMaxDb(int max)
{
    maxValue = max;
    maxByte1 = maxByte2 = 0x30;
    max = max / 10 % 10;
    maxByte1 += max;
    max = maxValue % 10;
    maxByte2 += max;
}

void AttenuatorPPC::disconnectFromAttenuator()
{
     HidUart_Close(devHandle);
     connectionState = false;
     emit disconnected();
}

AttenuatorPPC::~AttenuatorPPC()
{
    HidUart_Close(devHandle);
}

/*
QByteArray AttenuatorPPC::generateSendCommand(const QString &command,const QString &commandCheckSumm,const QString &hexATT)
{
    QByteArray send;
    QString sendHexStr; //посылаема команда в hex виде
    QString hiByte = hexATT.mid(2,2);
    QString lowByte = hexATT.mid(0,2);
    unsigned char checkSumm;

    checkSumm = attenuatorName.toInt(0,16) + commandCheckSumm.toInt(0,16) + hiByte.toInt(0,16) + lowByte.toInt(0,16);
    sendHexStr = "3a05" + attenuatorName + command + hexATT + QString::number(checkSumm,16) + "23";
    return send.fromHex(sendHexStr.toLocal8Bit());
}

void AttenuatorPPC::writeATT(const unsigned short &ATT)
{
        QString attCommand = "415454";//ATT в hex виде
        QString attCommandCheckSumm = "E9";
        QString hexATT = QString::number(ATT,10).toLocal8Bit().toHex(); //передоваемое числа в hex виде

        write(generateSendCommand(attCommand,attCommandCheckSumm,hexATT));

        if(readATN()){
            if(responseCode == 2){
                curValue = ATT;
                responseCode = 0;
            }
            else
                responseCode = 0;
            return;
        }
        disconnectFromAttenuator();
}

bool AttenuatorPPC::writeSTA()
{
        QString staCommand = "535441";//STA в hex виде
        QString staCommandCheckSumm = "e8";//заранее посчитанная контрольная сумма для команды STA
        QString hexATT = "3531"; //передоваемое числа в hex виде

        write(generateSendCommand(staCommand,staCommandCheckSumm,hexATT));

        if(readATN())
            if(responseCode == 6){
                responseCode = 0;
                return true;
            }
            else{
                responseCode = 0;
                return false;
            }
        else
            return false;

}

void AttenuatorPPC::write(const QByteArray &send)
{
    if(!serialPort.isOpen()){
        emit errorMSG("port error");
        return;
    }
    serialPort.write(send);
    serialPort.waitForBytesWritten(TIMEOUT);
}

bool AttenuatorPPC::readATN()
{
    QByteArray data;
    QStringList dataSplit;
    QByteArray endStr;
    endStr = endStr.fromHex("40");
    minValue = maxValue = responseCode = curValue = codeFreqDiap = 0;
        while (serialPort.waitForReadyRead(TIMEOUT))
        {//Пока не считается все, ждем
            data += serialPort.readAll();
            if (data.endsWith(endStr))//читаем до прихода конца посылки
            {
                    if(VeryfiCRC(data,dataSplit)){
                        if(attenuatorName != dataSplit[1]){
                            emit errorMSG("attenuator address incorrect");
                            return false;
                        }
                        else
                        {
                            minValue = dataSplit[3].toInt(0,16);
                            maxValue = dataSplit[4].toInt(0,16);
                            codeFreqDiap = dataSplit[5].toInt(0,16);
                            curValue = dataSplit[6].toInt(0,16);
                            responseCode = dataSplit[2].toInt(0,16);
                            initInformation ="Address: " + attenuatorName + "\n" +
                                             "Min: " + QString::number(minValue) + " dB\n" +
                                             "Max: " + QString::number(maxValue) + " dB\n";
                            return true;
                        }
                    }
                    return false;//Если все в порядке - возвращаем нашу посылку
            }
        }
        data.clear();
        emit errorMSG("no response");
        return false;
}

bool AttenuatorPPC::VeryfiCRC(const QByteArray &data,QStringList &dataSplit)
{
    //Процедура проверки контрольной суммы принятого сообщения
    int lenght = data.size();
    if(lenght == 11){ //проверка длины, сообшения

        dataSplit.clear();
        //берем первые 3 байта и смотрим название команды
        dataSplit << data.toHex().mid(0,6);
       // разбивка ответа
        for(int i = 6;i < data.toHex().size();i = i + 2)
            dataSplit << data.toHex().mid(i,2);

       //подсчет контрольной суммы
        unsigned char checkSumm = 0;
        for(int i = 1; i < 7; i++)
            checkSumm += dataSplit[i].toInt(0,16);
        if(checkSumm == dataSplit[7].toInt(0,16)){
            return true;
        }
        else{
            emit errorMSG("checksum error");
            return false;
        }
    }
    else{
        emit errorMSG("length error");
        return false;
    }
}*/
