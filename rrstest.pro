#-------------------------------------------------
#
# Project created by QtCreator 2015-01-27T17:36:11
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = rrsTest
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
        snmpsimple.cpp \
        widgets/connectinwindow.cpp \
        widgets/connectwidget.cpp \
        widgets/attconnectwidget.cpp \
        widgets/testwindow.cpp \
        widgets/dlinkwidget.cpp \
        dlink/dlink3120new.cpp \
        RRC/radiorelaynew.cpp \
        attenuator/attenuatorppc.cpp \
        channelprocessing.cpp \
        config.cpp \
    widgets/testWindowWidgets/testparamwidget.cpp \
    widgets/testWindowWidgets/resultwidget.cpp \
    test.cpp \
    widgets/parameterswindow.cpp \
    widgets/paramsWindowWidgets/attparamwidget.cpp \
    widgets/paramsWindowWidgets/heatparamwidget.cpp \
    widgets/paramsWindowWidgets/socketsparamwidget.cpp

HEADERS  += mainwindow.h \
        snmpsimple.h \
        widgets/connectinwindow.h \
        widgets/connectwidget.h \
        widgets/attconnectwidget.h \
        widgets/testwindow.h \
        widgets/dlinkwidget.h \
        dlink/dlink3120new.h \
        RRC/radiorelaynew.h \
        attenuator/attenuatorppc.h \
        channelprocessing.h \
        config.h \
    widgets/testWindowWidgets/testparamwidget.h \
    widgets/testWindowWidgets/resultwidget.h \
    test.h \
    widgets/parameterswindow.h \
    widgets/paramsWindowWidgets/attparamwidget.h \
    widgets/paramsWindowWidgets/heatparamWidget.h \
    widgets/paramsWindowWidgets/socketsparamwidget.h

win32: LIBS += -L$$PWD/../../_myLibs/snmpp/lib/ -lsnmp++ -lws2_32
INCLUDEPATH += $$PWD/../../_myLibs/snmpp/include
DEPENDPATH += $$PWD/../../_myLibs/snmpp/include
win32:win32-g++: PRE_TARGETDEPS += $$PWD/../../_myLibs/snmpp/lib/libsnmp++.a

win32: LIBS += -L$$PWD/../../_myLibs/quazip/lib -lquazip
INCLUDEPATH += $$PWD/../../_myLibs/quazip/include

# SiLabs HID to UART library
LIBS += -L$$PWD/../../_myLibs/siusbcp211x/lib/ -lSLABHIDtoUART
INCLUDEPATH += $$PWD/../../_myLibs/siusbcp211x/include
DEPENDPATH += $$PWD/../../_myLibs/siusbcp211x/include

DISTFILES +=


