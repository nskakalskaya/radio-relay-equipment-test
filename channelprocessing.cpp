#include "channelprocessing.h"
#include "QMessageBox"

///TODO
/// вместо логов писать нет ошибок и писать лог, если есть ошибки+
/// закрывать лог файл после остановки+
/// имя файла - модель, серийный номер, время старта+
/// архивирование+
ChannelProcessing::ChannelProcessing(const unsigned int &numChannel, const unsigned int &numPort, Config &cnf, QObject *parent) : QObject(parent),
    testIsFinished(false),
    isNeedToCompress(false)
{
    receiveRRCInfo[0] = receiveRRCInfo[1] = false;
    receiveDlinkInfo[0] = receiveDlinkInfo[1] = false;
    dirName = "Channel № " + QString::number(numChannel) + " ( port № " + QString::number(numPort) + ")";
    perBorder = cnf.getPerBorder();
    lostBorder = cnf.getLostBorder();

}

void ChannelProcessing::start()
{
    int oneDay = 24 * 60 * 60 * 1000;

    shutDownTimer = new QTimer(this);
    shutDownTimer->setInterval(oneDay);
    shutDownTimer->setSingleShot(true);

    QDir dir;
    if(!dir.exists(dirName))
        dir.mkpath(dirName);

    logFile = new QFile(this);

    connect(shutDownTimer,&QTimer::timeout,this,ChannelProcessing::stopChannel);
}


void ChannelProcessing::initialize()
{
    colorChannel = "green";
    emit colorStatus(QColor(colorChannel));
    double zero = 0;
    for(int i = 0; i < 2; i++){
        totalRRCRXPacket[i] = 0;
        totalRRCTXPacket[i] = 0;
        totalDlinkRXPacketGood[i] = 0;
        totalDlinkErrors[i] = 0;
        totalDlinkTXPacket[i] = 0;
        startError[i] = -1;
        numStatusRRC[i] = 1;
        numStatusDlink[i] = 1;
        //lostPacket[i] = 0;
        emit rrcPERSignal(i,zero);
        emit dlinkPERSignal(i,zero);
        emit totalDlinkPERSignal(i,zero);
        emit lostDlinkSignal(i,zero);
        emit setShutDownTime("0");
    }
    testIsFinished = false;
    timeDuration.start = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss");
}

void ChannelProcessing::getRRCInfo(RRCInfo rrcInfo) //слот обработки информации
{
    LogStruct logStruct;
    int numRadio = rrcInfo.numRadio;
   // quint64 deltaError = 0;//текущее количетсво ошибок
   // double PER = 0; //текущее значение PER

    receiveRRCInfo[numRadio] = true;

    this->rrcInfo[numRadio] = rrcInfo;////del относится к log
    logStruct.rrc = rrcInfo;

    totalRRCRXPacket[numRadio] += rrcInfo.RXPacketsPerSec.toULong();//подсчет общего количества принятых пакетов
    totalRRCTXPacket[numRadio] += rrcInfo.TXPacketsPerSec.toULong();

//    if(startError[numRadio] == -1)
//        startError[numRadio] = rrcInfo.fecFailTotal.toULong();

 //   deltaError = rrcInfo.fecFailTotal.toULong() - startError[numRadio]; // число ошибок с начала теста

    /*подсчет перов*/
//    if( totalRRCRXPacket[numRadio] != 0)
//        PER = deltaError / (long double)totalRRCRXPacket[numRadio];

  //  logStruct.totalRRCTXPacket = QString::number(totalRRCTXPacket[numRadio]);
 //   logStruct.totalRRCRXPacket = QString::number(totalRRCRXPacket[numRadio]);
 ////   logStruct.PER = QString::number(PER);
 //   logStruct.deltaError = QStri//ng::number(deltaError);

  //  emit rrcPERSignal(numRadio,PER);
    //numStatusRRC[numRadio] = checkPER(PER);//оценка перов
    //logPER[numRadio] = PER;
    //logError[numRadio] = deltaError;

    rrcLog[numRadio].push_back(logStruct);

}

void ChannelProcessing::getDlinkInfo(DlinkInfo dlinkInfo)
{
    RXPacketsGOOD[dlinkInfo.numDlink] = dlinkInfo.RXPackets.toULongLong() - dlinkInfo.RXDrop.toULongLong();//количество ринятых пакетов без дропов
    DlinkErrors[dlinkInfo.numDlink] = dlinkInfo.RXCRCError.toULong() + dlinkInfo.RXUnderSize.toULong() +//сумма ошибок
            dlinkInfo.RXOverSize.toULong() + dlinkInfo.RXFragments.toULong() + dlinkInfo.RXJabber.toULong() + dlinkInfo.RXSymbolError.toULong();

    emit toLog("GET dlinkInfo.numDlink " + QString::number(dlinkInfo.numDlink));

    double PER = 0;
    if(RXPacketsGOOD > 0)
        PER = DlinkErrors[dlinkInfo.numDlink] / (long double)RXPacketsGOOD[dlinkInfo.numDlink];

    emit toLog("RXPacketsGOOD[dlinkInfo.numDlink] " + QString::number(RXPacketsGOOD[dlinkInfo.numDlink]));

    this->dlinkInfo[dlinkInfo.numDlink] = dlinkInfo;
    receiveDlinkInfo[dlinkInfo.numDlink] = true;

    totalDlinkRXPacketGood[dlinkInfo.numDlink] += RXPacketsGOOD[dlinkInfo.numDlink];
    totalDlinkTXPacket[dlinkInfo.numDlink] += dlinkInfo.TXPackets.toULongLong();
    totalDlinkErrors[dlinkInfo.numDlink] += DlinkErrors[dlinkInfo.numDlink];

    emit dlinkPERSignal(dlinkInfo.numDlink,PER);

    if (receiveDlinkInfo[0] && receiveDlinkInfo[1]) { //если получена информация с обоих Dlink
        timeDuration.stop = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss");
        receiveDlinkInfo[0] = receiveDlinkInfo[1] = false;

        double curLost[2];//потерянные пакеты за текущий проход
        curLost[1] = qAbs(this->dlinkInfo[0].TXPackets.toULong() - RXPacketsGOOD[1]) / (double)RXPacketsGOOD[1];
        curLost[0] = qAbs(this->dlinkInfo[1].TXPackets.toULong() - RXPacketsGOOD[0]) / (double)RXPacketsGOOD[0];

        emit toLog("all info received");
        emit toLog("dlinkInfo[1].TXPackets" + this->dlinkInfo[1].TXPackets);
        emit toLog("dlinkInfo[0].TXPackets" + this->dlinkInfo[0].TXPackets);

        emit toLog("curelost[1]" + QString::number(curLost[1]));
        emit toLog("curelost[0]" + QString::number(curLost[0]));

        checkChannelColorStatus(); //оценкa цвета канала
        if (!logFile->isOpen()) {
            openLogFile();
        }

        if (DlinkErrors[0] > 0 || DlinkErrors[1] > 0 || curLost[0] > 0 || curLost[1] > 0) {
            writeRRCInfo();
            writeDlinkInfo();
        }
        else {
            QTextStream out(logFile);
            out << timeDuration.start << " - " << timeDuration.stop << " No errors\n";
        }

        rrcLog[0].clear();
        rrcLog[1].clear();

        timeDuration.start = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss");

        if (testIsFinished) {
            emit resultComlpete();
            closeLogFile();
        }
    }
}

void ChannelProcessing::writeDlinkInfo()
{  

    if(!logFile->isOpen())
        return;

    QTextStream out(logFile);
    out.setFieldWidth(20);
    out.setFieldAlignment(QTextStream::AlignRight);
    out << "color" << "RX packets(GOOD)" << "TX packets" << "RX total Packet"  <<
               "TX total packets" << "total error" << "RX CRC error" << "RX under size" << "RX over size" <<
               "RX fragments" << "RX jabber" << "RX drop" << "RX symbol error" << "TX exdefer" <<
               "TX late coll" << "TX ex coll" << "tx sing coll" << "TX collision" << "||";

    out << "color" << "RX packets(GOOD)" << "TX packets" << "RX total Packet"  <<
               "TX total packets" << "total error" << "RX CRC error" << "RX under size" << "RX over size" <<
               "RX fragments" << "RX jabber" << "RX drop" << "RX symbol error" << "TX exdefer" <<
               "TX late coll" << "TX ex coll" << "tx sing coll" << "TX collision\n";

    QString dlinkRXPacketsGood[2];
    dlinkRXPacketsGood[0] = QString::number(dlinkInfo[0].RXPackets.toULongLong() - dlinkInfo[0].RXDrop.toULongLong());
    dlinkRXPacketsGood[1] = QString::number(dlinkInfo[1].RXPackets.toULongLong() - dlinkInfo[1].RXDrop.toULongLong());

    out << colorChannel  << dlinkRXPacketsGood[0] << dlinkInfo[0].TXPackets  << QString::number(totalDlinkRXPacketGood[0])
            << QString::number(totalDlinkTXPacket[0]) << QString::number(totalDlinkErrors[0])  << dlinkInfo[0].RXCRCError  << dlinkInfo[0].RXUnderSize
            << dlinkInfo[0].RXOverSize << dlinkInfo[0].RXFragments  << dlinkInfo[0].RXJabber  << dlinkInfo[0].RXDrop  << dlinkInfo[0].RXSymbolError
            << dlinkInfo[0].TXExDefer  << dlinkInfo[0].TXLateColl  << dlinkInfo[0].TXExColl  << dlinkInfo[0].TXSingColl
            << dlinkInfo[0].TXCollision << "||";

    out << colorChannel  << dlinkRXPacketsGood[1]  << dlinkInfo[1].TXPackets  << QString::number(totalDlinkRXPacketGood[1])
            << QString::number(totalDlinkTXPacket[1]) << QString::number(totalDlinkErrors[1]) << dlinkInfo[1].RXCRCError  << dlinkInfo[1].RXUnderSize
            << dlinkInfo[1].RXOverSize << dlinkInfo[1].RXFragments  << dlinkInfo[1].RXJabber  << dlinkInfo[1].RXDrop  << dlinkInfo[1].RXSymbolError
            << dlinkInfo[1].TXExDefer  << dlinkInfo[1].TXLateColl  << dlinkInfo[1].TXExColl  << dlinkInfo[1].TXSingColl
            << dlinkInfo[1].TXCollision << "\n";
}

void ChannelProcessing::writeRRCInfo()
{
    if(!logFile->isOpen())
        return;

    int size;

    if (rrcLog[0].size() >= rrcLog[1].size())
        size = rrcLog[1].size();
    else
        size = rrcLog[0].size();

    QTextStream out(logFile);
    out.setFieldWidth(20);
    out.setFieldAlignment(QTextStream::AlignRight);

    if (rrcInfo[0].gigabyteModel){
        out <<"Time" << "TS1" << "TS2" << "AGC" << "RSL" << "U-9V"
            << "U+5V" << "U+5VSB" << "CONS" << "Radio" << "Heater" << "RX[p/s]"
            << "TX[p/s]" << "SNR" << "Downlink" << "Uplink" << "CRC"
            << "Delta Error" << "PER" << "||";
        out <<"Time" << "TS1" << "TS3" << "AGC" << "RSL" << "U-9V"
            << "U+5V" << "U+5VSB" << "CONS" << "Radio" << "Heater" << "RX[p/s]"
            << "TX[p/s]" << "SNR" << "Downlink" << "Uplink" << "CRC"
            << "\n";
//        QFile test("11.txt");
//        test.open(QIODevice::Append);
//        QTextStream outer(&test);

        for (int i = 0; i < size; i++) {
//            outer << i << " size " << size << endl;
            out << rrcLog[0][i].rrc.time
                << rrcLog[0][i].rrc.tempSensor1
                << rrcLog[0][i].rrc.tempSensor2
                << rrcLog[0][i].rrc.UAG
                << rrcLog[0][i].rrc.ATT
                << rrcLog[0][i].rrc.UMIN12V
                << rrcLog[0][i].rrc.U5V
                << rrcLog[0][i].rrc.U5VSB
                << rrcLog[0][i].rrc.consumption
                << QString::number(rrcLog[0][i].rrc.radioStatus)
                << QString::number(rrcLog[0][i].rrc.heaterStatus)
                << rrcLog[0][i].rrc.RXPacketsPerSec
                << rrcLog[0][i].rrc.TXPacketsPerSec
                << rrcLog[0][i].rrc.URE
                << rrcLog[0][i].rrc.downlink
                << rrcLog[0][i].rrc.uplink
                << rrcLog[0][i].rrc.MSE << "||";

            out << rrcLog[1][i].rrc.time
                    << rrcLog[1][i].rrc.tempSensor1
                    << rrcLog[1][i].rrc.tempSensor2
                    << rrcLog[1][i].rrc.UAG
                    << rrcLog[1][i].rrc.ATT
                    << rrcLog[1][i].rrc.UMIN12V
                    << rrcLog[1][i].rrc.U5V
                    << rrcLog[1][i].rrc.U5VSB
                    << rrcLog[1][i].rrc.consumption
                    << QString::number(rrcLog[1][i].rrc.radioStatus)
                    << QString::number(rrcLog[1][i].rrc.heaterStatus)
                    << rrcLog[1][i].rrc.RXPacketsPerSec
                    << rrcLog[1][i].rrc.TXPacketsPerSec
                    << rrcLog[1][i].rrc.URE
                    << rrcLog[1][i].rrc.downlink
                    << rrcLog[1][i].rrc.uplink
                    << rrcLog[1][i].rrc.MSE << "\n";
        }
    }
    else {
        out <<"Time" << "TS1" << "TS2" << "AGC" << "RSL" << "U-9V"
            << "U+5V" << "U+5VSB" << "CONS" << "Radio" << "Heater" << "RX[p/s]"
            << "TX[p/s]" << "SNR" << "Downlink" << "Uplink" << "CRC" << "||";
        out <<"Time" << "TS1" << "TS3" << "AGC" << "RSL" << "U-9V"
            << "U+5V" << "U+5VSB" << "CONS" << "Radio" << "Heater" << "RX[p/s]"
            << "TX[p/s]" << "SNR" << "Downlink" << "Uplink" << "CRC"
            << "\n";
//        out <<"Time" << "TS1" << "TS2" << "TS3" << "URE" << "UTR" << "UAG" << "ATT"
//            << "U24V" << "U+10V" << "U-12V" << "U+5V" << "U+5VSB" << "Curr"
//            << "ONP" << "ON1" << "PHY->R[pps]" << "PHY->R" << "R->PHY[pps]" << "R->PHY"
//            << "Error" << "PER" << "DropRX" << "DropTX" << "UpLink_Mbps" << "DownLink_Mbps"
//            << "MSE_RX" << "Frequency_Offset_Hz" << "||";
//        out <<"Time" << "TS1" << "TS2" << "TS3" << "URE" << "UTR" << "UAG" << "ATT"
//            << "U24V" << "U+10V" << "U-12V" << "U+5V" << "U+5VSB" << "Curr"
//            << "ONP" << "ON1" << "PHY->R[pps]" << "PHY->R" << "R->PHY[pps]" << "R->PHY"
//            << "Error" << "PER" << "DropRX" << "DropTX" << "UpLink_Mbps" << "DownLink_Mbps"
//            << "MSE_RX" << "Frequency_Offset_Hz" << "\n";

//        for (int i = 0; i < size; i++) {
//            out << rrcLog[0][i].rrc.time  << rrcLog[0][i].rrc.tempSensor1  << rrcLog[0][i].rrc.tempSensor2  << rrcLog[0][i].rrc.tempSensor3
//                << rrcLog[0][i].rrc.URE << rrcLog[0][i].rrc.UTR << rrcLog[0][i].rrc.UAG << rrcLog[0][i].rrc.ATT << rrcLog[0][i].rrc.U24V
//                << rrcLog[0][i].rrc.UPLUS10V << rrcLog[0][i].rrc.UMIN12V << rrcLog[0][i].rrc.U5V << rrcLog[0][i].rrc.U5VSB << rrcLog[0][i].rrc.consumption
//                << QString::number(rrcLog[0][i].rrc.radioStatus) << QString::number(rrcLog[0][i].rrc.heaterStatus) << rrcLog[0][i].rrc.RXPacketsPerSec
//                << rrcLog[0][i].totalRRCRXPacket << rrcLog[0][i].rrc.TXPacketsPerSec << rrcLog[0][i].totalRRCTXPacket << rrcLog[0][i].deltaError
//                << rrcLog[0][i].PER << rrcLog[0][i].rrc.dropRXpackets  << rrcLog[0][i].rrc.dropTXpackets << rrcLog[0][i].rrc.uplink  << rrcLog[0][i].rrc.downlink
//                << rrcLog[0][i].rrc.MSE << rrcLog[0][i].rrc.offsetHz <<  "||";

//            out << rrcLog[1][i].rrc.time  << rrcLog[1][i].rrc.tempSensor1  << rrcLog[1][i].rrc.tempSensor2  << rrcLog[1][i].rrc.tempSensor3
//                << rrcLog[1][i].rrc.URE << rrcLog[1][i].rrc.UTR << rrcLog[1][i].rrc.UAG << rrcLog[1][i].rrc.ATT << rrcLog[1][i].rrc.U24V
//                << rrcLog[1][i].rrc.UPLUS10V << rrcLog[1][i].rrc.UMIN12V << rrcLog[1][i].rrc.U5V << rrcLog[1][i].rrc.U5VSB << rrcLog[1][i].rrc.consumption
//                << QString::number(rrcLog[1][i].rrc.radioStatus) << QString::number(rrcLog[1][i].rrc.heaterStatus) << rrcLog[1][i].rrc.RXPacketsPerSec
//                << rrcLog[1][i].totalRRCRXPacket << rrcLog[1][i].rrc.TXPacketsPerSec << rrcLog[1][i].totalRRCTXPacket << rrcLog[1][i].deltaError
//                << rrcLog[1][i].PER << rrcLog[1][i].rrc.dropRXpackets  << rrcLog[1][i].rrc.dropTXpackets << rrcLog[1][i].rrc.uplink  << rrcLog[1][i].rrc.downlink
//                << rrcLog[1][i].rrc.MSE << rrcLog[1][i].rrc.offsetHz <<  "\n";
//        }
        for (int i = 0; i < size; i++){
            out << rrcLog[0][i].rrc.time
                << rrcLog[0][i].rrc.tempSensor1
                << rrcLog[0][i].rrc.tempSensor2
                << rrcLog[0][i].rrc.UAG
                << rrcLog[0][i].rrc.ATT
                << rrcLog[0][i].rrc.UMIN12V
                << rrcLog[0][i].rrc.U5V
                << rrcLog[0][i].rrc.U5VSB
                << rrcLog[0][i].rrc.consumption
                << QString::number(rrcLog[0][i].rrc.radioStatus)
                << QString::number(rrcLog[0][i].rrc.heaterStatus)
                << rrcLog[0][i].rrc.RXPacketsPerSec
                << rrcLog[0][i].rrc.TXPacketsPerSec
                << rrcLog[0][i].rrc.URE
                << rrcLog[0][i].rrc.downlink
                << rrcLog[0][i].rrc.uplink
                << rrcLog[0][i].rrc.MSE << "||";

            out << rrcLog[1][i].rrc.time
                    << rrcLog[1][i].rrc.tempSensor1
                    << rrcLog[1][i].rrc.tempSensor2
                    << rrcLog[1][i].rrc.UAG
                    << rrcLog[1][i].rrc.ATT
                    << rrcLog[1][i].rrc.UMIN12V
                    << rrcLog[1][i].rrc.U5V
                    << rrcLog[1][i].rrc.U5VSB
                    << rrcLog[1][i].rrc.consumption
                    << QString::number(rrcLog[1][i].rrc.radioStatus)
                    << QString::number(rrcLog[1][i].rrc.heaterStatus)
                    << rrcLog[1][i].rrc.RXPacketsPerSec
                    << rrcLog[1][i].rrc.TXPacketsPerSec
                    << rrcLog[1][i].rrc.URE
                    << rrcLog[1][i].rrc.downlink
                    << rrcLog[1][i].rrc.uplink
                    << rrcLog[1][i].rrc.MSE << "\n";
        }
    }
    out.flush();
}


quint16 ChannelProcessing::checkPER(double &PER)
{
    if(PER >= 0 && PER < perBorder.yellowBorder)
        return 1;

    if(PER >= perBorder.yellowBorder && PER < perBorder.redBorder)
        return 2;

    if(PER >= perBorder.redBorder)
        return 3;
}

quint16 ChannelProcessing::checkLostStatus(const double &lost)
{
    if(lost >= 0 && lost < lostBorder.yellowBorder)
        return 1;

    if(lost >= lostBorder.yellowBorder && lost < lostBorder.redBorder)
        return 2;

    if(lost >= lostBorder.redBorder)
        return 3;
}

void ChannelProcessing::checkChannelColorStatus()
{
    emit toLog("check channel color status");
    /*получение цифрового статуса релеек по перам*/
    double totalPER[2] = {0,0};
    double lostPacket[2] = {0,0};
    if(totalDlinkRXPacketGood[0] > 0)
        totalPER[0] = totalDlinkErrors[0] / (long double)totalDlinkRXPacketGood[0];

    if(totalDlinkRXPacketGood[1] > 0)
        totalPER[1] = totalDlinkErrors[1] / (long double)totalDlinkRXPacketGood[1];

    emit totalDlinkPERSignal(0,totalPER[0]);
    emit totalDlinkPERSignal(1,totalPER[1]);

    numStatusDlink[0] = checkPER(totalPER[0]);
    numStatusDlink[1] = checkPER(totalPER[1]);

    emit toLog("numStatusDlink[0] " + numStatusDlink[0]);
    emit toLog("numStatusDlink[1] " + numStatusDlink[1]);

    /*получение цифрового статуса релеек по потерянным пакета*/    

    lostPacket[0] = qAbs(totalDlinkTXPacket[1] - totalDlinkRXPacketGood[0]) / (double)totalDlinkRXPacketGood[0];

    lostPacket[1] = qAbs(totalDlinkTXPacket[0] - totalDlinkRXPacketGood[1]) / (double)totalDlinkRXPacketGood[1];

    emit toLog("COUNT LOST");

    emit toLog("totalDlinkTXPacket[1] " + QString::number(totalDlinkTXPacket[1]));
    emit toLog("totalDlinkTXPacket[0] " + QString::number(totalDlinkTXPacket[0]));
    emit toLog("totalDlinkRXPacketGood[1] " + QString::number(totalDlinkRXPacketGood[1]));
    emit toLog("totalDlinkRXPacketGood[0] " + QString::number(totalDlinkRXPacketGood[0]));
    emit toLog("qAbs[1] " + QString::number(qAbs(totalDlinkTXPacket[1] - totalDlinkRXPacketGood[0])));
    emit toLog("qAbs[0] " + QString::number(qAbs(totalDlinkTXPacket[0] - totalDlinkRXPacketGood[1])));
    emit toLog("lostPacket[1] " + QString::number(lostPacket[1]));
    emit toLog("lostPacket[0] " + QString::number(lostPacket[0]));

    numStatusLostDlink[0] = checkLostStatus(lostPacket[0]);
    numStatusLostDlink[1] = checkLostStatus(lostPacket[1]);

    emit lostDlinkSignal(0,lostPacket[0]);
    emit lostDlinkSignal(1,lostPacket[1]);

    quint16 numStatusChannel = 0;//цифровой статус канала
    QString nextColorChannel;

    /*получение цифрового статуса канала(поиск максимума)*/
    for(int i = 0; i < 2; i++){
        /*if(numStatusRRC[i] > numStatusChannel)
            numStatusChannel = numStatusRRC[i];*////статистика по релейкам не участвует в определении цвета канала

        if(numStatusDlink[i] > numStatusChannel)
            numStatusChannel = numStatusDlink[i];

        if(numStatusLostDlink[i] > numStatusChannel)
            numStatusChannel = numStatusLostDlink[i];
    }

    emit toLog("numStatusChannel " + QString::number(numStatusChannel));

    /*перевод из цифрового статуса, в цветовой, и принятие решений по запуску и остановке таймера*/
    if(numStatusChannel == 1){
        nextColorChannel = "green";
        if(shutDownTimer->isActive())
            shutDownTimer->stop();

        emit setShutDownTime("0");
    }
    /*if(numStatusChannel == 2 || numStatusChannel == 3){*/
    if(numStatusChannel == 2)
        nextColorChannel = "yellow";

    if(numStatusChannel == 3){
        nextColorChannel = "red";

        if(!shutDownTimer->isActive()) // если таймер выключения не активен, то запускаем его
            shutDownTimer->start();

        emit toLog("start shutdown timer");
        emit setShutDownTime(QTime::fromMSecsSinceStartOfDay(shutDownTimer->remainingTime()).toString("hh:mm:ss"));//обновляем статус таймера
    }

    if(nextColorChannel != colorChannel){
        emit colorStatus(QColor(nextColorChannel));
        colorChannel = nextColorChannel;
    }
}

void ChannelProcessing::testFinished()
{
    emit toLog("Test is Finished");
    testIsFinished = true;
}

void ChannelProcessing::closeFile()
{
    closeLogFile();
}


bool ChannelProcessing::openLogFile()
{
    QDir dir;
    if(!dir.exists(dirName))
        dir.mkpath(dirName);


    fullPathName = dirName + "/" + serialname + " "  + QDateTime::currentDateTime().toString("dd.MM.yyyy hh.mm.ss");

//    fullPathName.replace("\r","");
//    fullPathName.replace("\n","");

    logFile->setFileName(fullPathName + ".log");
    if (!logFile->open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    return true;
}


void ChannelProcessing::closeLogFile()
{
    if(logFile->isOpen())
        logFile->close();

    /*сжатие файла*/
    if(isNeedToCompress){
        if(fullPathName.isEmpty())
            return;

        QStringList lst;
        lst << fullPathName + ".log";
        JlCompress::compressFiles(fullPathName + ".zip",lst);
        fullPathName.clear();
    }
}

void ChannelProcessing::getModelName(QString modelname)
{
    serialname = modelname;
}

void ChannelProcessing::setModel(const int& numRadio,const QString& model)
{
    this->model[numRadio] = model;
}

void ChannelProcessing::setSerialNumber(const int& numRadio,const QString& serialNumber)
{
    this->serialNumber[numRadio] = serialNumber;    
}

void ChannelProcessing::setComress(bool state)
{
    isNeedToCompress = state;
}

ChannelProcessing::~ChannelProcessing()
{
    closeFile();
}

