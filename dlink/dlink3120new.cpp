#include "dlink3120new.h"
#include <QTime>

Dlink3120new::Dlink3120new(const unsigned int &numDlink, QObject *parent) :
    SnmpSimple(parent),
    numDlink(numDlink)
{
    port = 161;
    //interval = 100;
    readCommunityName = "public";
    writeCommunityName = "private";
    connectStat = false;

}

void Dlink3120new::connectToSnmp(const QString &ipaddr)
{
    address = ipaddr.toLocal8Bit().data();
    address.set_port(port);


    if(initializeSnmp())
        if(checkConnect())
            emit connected();
    getStat();
}

void Dlink3120new::getStat()
{

    QVector <DlinkInfo> output;
    output.resize(numPorts.size());
    QStringList lst,res;
    for(int i = 0; i < numPorts.size(); i++){

       lst << getRXTotalPacketsOid(numPorts[i])
           << getTXTotalPacketsOid(numPorts[i])
           << getRXTotalBytesOid(numPorts[i])
           << getTXTotalBytesOid(numPorts[i])
           << getRXCRCErrorOid(numPorts[i])//8
           << getRXUnderSizeOid(numPorts[i])//9
           << getRXOverSizeOid(numPorts[i])//10
           << getRXFragmentsOid(numPorts[i])//11
           << getRXJabberOid(numPorts[i])//12
           << getRXDropOid(numPorts[i])//13
           << getRXSymbolErrorOid(numPorts[i])//14           
           << getTXExDeferOid(numPorts[i])//15
           << getTXExCollOid(numPorts[i]) //16
           << getTXLateCollOid(numPorts[i])//17
           << getTXSingCollOid(numPorts[i])//18
           << getTXCollisionOid(numPorts[i]);//19

       res = getSnmp(lst);

       if(res.size() != lst.size())
           return;

       output[i].port = numPorts[i];
       output[i].numDlink = numDlink;
       output[i].RXPackets = QString::number(res[0].toLongLong() + res[1].toLongLong() + res[2].toLongLong());
       output[i].TXPackets = QString::number(res[3].toLongLong() + res[4].toLongLong() + res[5].toLongLong());
       output[i].RXBytes = res[6];
       output[i].TXBytes = res[7];
               /*error Sector*/
       output[i].RXCRCError = res[8];
       output[i].RXUnderSize = res[9];
       output[i].RXOverSize = res[10];
       output[i].RXFragments = res[11];
       output[i].RXJabber = res[12];
       output[i].RXDrop = res[13];
       output[i].RXSymbolError = res[14];
       output[i].TXExDefer = res[15];
       output[i].TXLateColl = res[16];
       output[i].TXExColl = res[17];
       output[i].TXSingColl = res[18];
       output[i].TXCollision = res[19];
       output[i].time = QTime::currentTime().toString("hh:mm:ss");
       lst.clear();
    }

    qDebug() << "statFromDlink";
    emit statFromDlink(output);

}

void Dlink3120new::addPort(QString numPort)
{
    numPorts.push_back(numPort);
}

void Dlink3120new::addPortS(QStringList numPorts)
{
    this->numPorts = numPorts;
}

void Dlink3120new::cleanAllCounters()
{
    setSnmp("1.3.6.1.4.1.171.11.117.1.3.2.1.2.6.0",2);    
}

void Dlink3120new::turnOnPort(const QString &numPort)
{
    setSnmp("1.3.6.1.4.1.171.11.117.1.3.2.3.2.1.4." + numPort + ".2",3);
    QThread::sleep(4);
    emit portIsOn();
}

void Dlink3120new::turnOffPort(const QString& numPort)
{
    setSnmp("1.3.6.1.4.1.171.11.117.1.3.2.3.2.1.4." + numPort + ".2",2);
    QThread::sleep(10);
}

QStringList Dlink3120new::getRXTotalPacketsOid(QString numPort){
    QStringList res;
    res << QString("1.3.6.1.2.1.31.1.1.1.7." + numPort);
    res << QString("1.3.6.1.2.1.31.1.1.1.8." + numPort);
    res << QString("1.3.6.1.2.1.31.1.1.1.9." + numPort);
    return res;
}
QStringList Dlink3120new::getTXTotalPacketsOid(QString numPort){
    QStringList res;
    res << QString("1.3.6.1.2.1.31.1.1.1.11." + numPort);
    res << QString("1.3.6.1.2.1.31.1.1.1.12." + numPort);
    res << QString("1.3.6.1.2.1.31.1.1.1.13." + numPort);
    return res;
}
QString Dlink3120new::getRXTotalBytesOid(QString numPort){return QString("1.3.6.1.2.1.31.1.1.1.6." + numPort);}
QString Dlink3120new::getTXTotalBytesOid(QString numPort){return QString("1.3.6.1.2.1.31.1.1.1.10." + numPort);}
/*error Sector*/
QString Dlink3120new::getRXCRCErrorOid(QString numPort){return QString("1.3.6.1.2.1.16.1.1.1.8." + numPort);}
QString Dlink3120new::getRXUnderSizeOid(QString numPort){return QString("1.3.6.1.2.1.16.1.1.1.9." + numPort);}
QString Dlink3120new::getRXOverSizeOid(QString numPort){return QString("1.3.6.1.2.1.16.1.1.1.10." + numPort);}
QString Dlink3120new::getRXFragmentsOid(QString numPort){return QString("1.3.6.1.2.1.16.1.1.1.11." + numPort);}
QString Dlink3120new::getRXJabberOid(QString numPort){return QString("1.3.6.1.2.1.16.1.1.1.12." + numPort);}
QString Dlink3120new::getRXDropOid(QString numPort){return QString("1.3.6.1.2.1.16.1.1.1.3." + numPort);}
QString Dlink3120new::getRXSymbolErrorOid(QString numPort){return QString("1.3.6.1.2.1.10.7.2.1.18." + numPort);}
QString Dlink3120new::getTXExDeferOid(QString numPort){return QString("1.3.6.1.2.1.10.7.2.1.7." + numPort);}
QString Dlink3120new::getTXLateCollOid(QString numPort){return QString("1.3.6.1.2.1.10.7.2.1.8." + numPort);}
QString Dlink3120new::getTXExCollOid(QString numPort){return QString("1.3.6.1.2.1.10.7.2.1.9." + numPort);}
QString Dlink3120new::getTXSingCollOid(QString numPort){return QString("1.3.6.1.2.1.10.7.2.1.4." + numPort);}
QString Dlink3120new::getTXCollisionOid(QString numPort){return QString("1.3.6.1.2.1.10.7.2.1.5." + numPort);}

bool Dlink3120new::checkConnect()
{
    getSnmp("1.3.6.1.4.1.171.12.11.1.8.1.2");
    return connectStat;
}
