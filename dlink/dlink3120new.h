#ifndef DLINK3120NEW_H
#define DLINK3120NEW_H

#include "../snmpsimple.h"
#include <QThread>
#include <QTime>

struct DlinkInfo{
    int numDlink;
    QString port;
    QString RXPackets;
    QString TXPackets;
    QString RXBytes;
    QString TXBytes;
    /*error Sector*/
    QString RXCRCError;
    QString RXUnderSize;
    QString RXOverSize;
    QString RXFragments;
    QString RXJabber;
    QString RXDrop;
    QString RXSymbolError;
    QString TXExDefer;
    QString TXLateColl;
    QString TXExColl;
    QString TXSingColl;
    QString TXCollision;
    QString time;
};

class Dlink3120new : public SnmpSimple
{
    Q_OBJECT

    int numDlink;
    bool radioStat, gigabyteModel;
    QStringList numPorts; //номера портов с которых снимается лог

    QStringList getRXTotalPacketsOid(QString numPort);
    QStringList getTXTotalPacketsOid(QString numPort);
    QString getRXTotalBytesOid(QString numPort);
    QString getTXTotalBytesOid(QString numPort);
    /*error Sector*/
    QString getRXCRCErrorOid(QString numPort);
    QString getRXUnderSizeOid(QString numPort);
    QString getRXOverSizeOid(QString numPort);
    QString getRXFragmentsOid(QString numPort);
    QString getRXJabberOid(QString numPort);
    QString getRXDropOid(QString numPort);
    QString getRXSymbolErrorOid(QString numPort);
    QString getTXCRCErrorOid(QString numPort);
    QString getTXExDeferOid(QString numPort);
    QString getTXLateCollOid(QString numPort);
    QString getTXExCollOid(QString numPort);
    QString getTXSingCollOid(QString numPort);
    QString getTXCollisionOid(QString numPort);
    bool checkConnect();

public:
    explicit Dlink3120new(const unsigned int &numDlink,QObject *parent = 0);
    void addPort(QString numPort);
    void addPortS(QStringList numPorts);

signals:
    void statFromDlink(QVector<DlinkInfo> stat);
    void portIsOn();

public slots:
    void connectToSnmp(const QString &ipaddr);
    void getStat();
    void cleanAllCounters();
    void turnOffPort(const QString &numPort);
    void turnOnPort(const QString& numPort);
};

#endif // DLINK3120NEW_H
