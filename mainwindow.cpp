#include "mainwindow.h"
#include "snmpsimple.h"
#include <QDebug>

using namespace  std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      numOfDlink(2)
{

    QStringList port = config.getListenPort();
    //port << "3" << "4" << "5" << "6" << "7" << "8";

    if(port.size() == 0){
        QMessageBox msgBox;
        msgBox.setText("Ошибка.\nНе заданы прослушиваемые порты");
        msgBox.exec();
        exit(0);
    }

    QTabWidget *mainTabWidget = new QTabWidget(this);
    /*секция Dlink*/
    for(unsigned short i = 0; i < numOfDlink; i++){
        dlink3120.push_back(new Dlink3120new(i));
        dlink3120[i]->moveToThread(&dlinkThread[i]);
        dlink3120[i]->addPortS(port);
        connect(&dlinkThread[i], SIGNAL(started()),dlink3120[i],SLOT(start()));
        connect(&dlinkThread[i], SIGNAL(finished()),dlink3120[i], SLOT(deleteLater()));
    }
    /*конец Dlink*/

    pathToMIBFile = config.getMibPath();

    /*загрузка mib файла*/
    while(!loadMIBFile()){
        pathToMIBFile = QFileDialog::getOpenFileName(this,tr("Open MIB file"), "", tr("MIB File (*.mib)"));
        if(pathToMIBFile == "")//нажата кнопка cancel
            exit(0);
        config.setMibPath(pathToMIBFile);
    }



    /*секция релейки*/
    for(int i = 0; i < port.size(); i++) {
        for (int k = 0; k < 2; k++) {
            radioThread[k].push_back(new QThread(this));
            radioRelay[k].push_back(new RadioRelay(port[i].toInt(),k));
            radioRelay[k][i]->moveToThread(radioThread[k][i]);
            radioRelay[k][i]->writeSettings(snmpCommands, commandDescription);
            connect(radioThread[k][i], SIGNAL(started()),radioRelay[k][i],SLOT(start()));
            connect(radioThread[k][i], SIGNAL(finished()),radioRelay[k][i], SLOT(deleteLater()));
        }
    }
    /*конец релейки*/

    /*секция аттенюатора*/
    for (int i = 0; i < port.size(); i++) {
        attenuatorPPC.push_back(new AttenuatorPPC(this));
    }


    /*конец аттенюатора*/

    connectinWindow = new ConnectinWindow(port,dlink3120,radioRelay,attenuatorPPC,config,this);
    testWindow = new TestWindow(port,dlink3120,radioRelay,attenuatorPPC,config,this);
    paramsWindow = new ParametersWindow(port, radioRelay, attenuatorPPC,config,this);
    mainTabWidget->addTab(connectinWindow,"Connection");
    mainTabWidget->addTab(paramsWindow,"Parameters");
    mainTabWidget->addTab(testWindow,"Test");
    setCentralWidget(mainTabWidget);

    for(unsigned short i = 0; i < numOfDlink; i++)
        dlinkThread[i].start();

    for(int i = 0; i < port.size(); i++)
        for(int k = 0; k < 2; k++)
           radioThread[k][i]->start();

    connect(testWindow,&TestWindow::testActive,connectinWindow, &ConnectinWindow::blockDlinkConnect);
    connect(testWindow,&TestWindow::channelActive,connectinWindow, &ConnectinWindow::blockChannelConnect);
 //   connect(testWindow, &TestWindow::channelActive, paramsWindow, &ParametersWindow::blockAttWidgets);
    connect(testWindow, &TestWindow::testActive, paramsWindow, &ParametersWindow::disableHeat);
  //  connect(testWindow, &TestWindow::testActive, paramsWindow, &ParametersWindow::disableAtt);
    connect(testWindow, &TestWindow::testActive, paramsWindow, &ParametersWindow::disableSockets);

}

bool characterIsLetter(const char &in){
    if((in >= 'a' && in <= 'z') || (in >= 'A' && in <= 'Z'))
        return true;

    return false;
}

bool characterIsNum(const char &in){
    if(in >= '0' && in <= '9')
        return true;

    return false;
}

string trim(std::string &s) {
     s.erase(s.begin(), std::find_if_not(s.begin(), s.end(), [](char c){ return std::isspace(c); }));
     s.erase(std::find_if_not(s.rbegin(), s.rend(), [](char c){ return std::isspace(c); }).base(), s.end());
     return s;
}

string replaceSpace(const string& str)
{
    string inStr = str;//replace(str,'\t',' ');
    std::replace( inStr.begin(), inStr.end(), '\t', ' ');

    auto end = std::unique(inStr.begin(), inStr.end(), [](char l, char r){
        return std::isspace(l) && std::isspace(r) && l == r;
    });
    string ss = std::string(inStr.begin(), end);
    trim(ss);
    return ss;
}

bool lineContain(const string &str, const string &searchStr){
    return str.find(searchStr) != string::npos;
}



int findIntInString(const std::string &str,int &pos){
    std::string buf;
    for(size_t i = pos; i < str.length();i++){
        if(characterIsNum(str.at(i)))
            buf += str.at(i);
        else if(!buf.empty()){ //åñëè íàøëè íå öèôðó è ïðè ýòîì â áóôåðå ñîäåðæèòñÿ êàêîå-òî çíà÷åíèå, òî èñêîìîå ÷èñëî íàéäåíî
            pos = i + 1;
            bool ok = false;
            int number = stringToNumber<int>(buf,ok);
            return number;
        }
    }
    return 0;
}

int findIntInString(const std::string &str){
    int pos = 0;
    return findIntInString(str,pos);
}


std::string findWordInString(const std::string &str,int &pos){
    std::string buf;
    for(size_t i = pos; i < str.length();i++){
        if(characterIsNum(str.at(i)) || characterIsLetter(str.at(i))){
            buf += str.at(i);
        }
        else if(!buf.empty()){//åñëè íàøëè íå öèôðó èëè áóêâó è ïðè ýòîì â áóôåðå ñîäåðæèòñÿ êàêîå-òî çíà÷åíèå, òî èñêîìîå ñëîâî íàéäåíî
            pos = i + 1;
            return buf;
        }
    }
    return "";
}
std::string findWordInString(const std::string &str){
    int pos = 0;
    return findWordInString(str,pos);
}

bool getGroupAndAdress(const std::string &line,std::string &rootName, OIDTYPE &address){
    size_t searchPos = line.find('{');//ïîçèöèÿ îòêóäà ñëåäóåò èñêàòü
    if(searchPos != std::string::npos){
        int pos = searchPos;
        address = findIntInString(line,pos);
        pos = searchPos;
        rootName = findWordInString(line,pos);
        return true;
    }
    return false;
}

inline bool setGroup(const string& parentName, const string& groupName,const OIDTYPE &oidAddr,map <string, OIDARR> &groups){

    auto oidIt = groups.find(parentName);
    if (oidIt != groups.end()) {
        OIDARR oidarr = (*oidIt).second;
        oidarr.push_back(oidAddr);
        groups.insert( make_pair(groupName,oidarr));
        return true;
    }
    return false;
}

bool searchEnterprises(vector <string>::iterator &line, vector <string>::iterator &endLine,map <string, OIDARR> &groups){

    string trimLine = replaceSpace(*line);
    if(lineContain(trimLine,"MODULE-IDENTITY") && !lineContain(trimLine,"FROM SNMPv2-SMI")){
        string groupName = findWordInString(trimLine);
        for (; line != endLine; line++) {
            string trimLine2 = replaceSpace(*line);
            if (lineContain(trimLine2,"enterprises")) {
                string parentName;
                OIDTYPE oid;
                if (getGroupAndAdress(trimLine2,parentName,oid)) {
                    return setGroup(parentName,groupName,oid,groups);
                }
            }
        }
    }
    else {
       if (lineContain(trimLine,"OBJECT IDENTIFIER")) {
           string groupName = findWordInString(trimLine);
           string parentName;
           OIDTYPE oid;
           if (getGroupAndAdress(trimLine,parentName,oid)) {
               return setGroup(parentName,groupName,oid,groups);
           }
       }
    }
    return false;
}

bool searchOid(vector <string>::iterator &line, vector <string>::iterator &endLine,const map <string, OIDARR> &groups,list <OidIdentifier> &oidIdent,const string &oidStrType) {
    string trimLine = replaceSpace(*line);
    if (lineContain(trimLine,oidStrType)) {
        string groupName = findWordInString(trimLine);
        for (; line != endLine; line++) {
            string trimLine2 = replaceSpace(*line);
            if (lineContain(trimLine2,"::=")) {
                string parentName;
                OIDTYPE oid;
                if (getGroupAndAdress(trimLine2,parentName,oid)) {
                    auto oidIt = groups.find(parentName);
                    if (oidIt != groups.end()) {
                        OidIdentifier tmpIdent;
                        tmpIdent.name = groupName;
                        tmpIdent.oid = (*oidIt).second;
                        tmpIdent.oid.push_back(oid);
                        oidIdent.push_back(tmpIdent);
                        return true;
                    }
                }
                return false;
            }
        }
    }
    return false;
}




bool searchGroupNames(const string &line,map <string, OIDARR> &groups) {

    string trimLine = replaceSpace(line);
    if (lineContain(trimLine,"OBJECT IDENTIFIER")) {
        string groupName = findWordInString(trimLine);
        string parentName;
        OIDTYPE oid;
        if (getGroupAndAdress(trimLine,parentName,oid)) {
            return setGroup(parentName,groupName,oid,groups);
        }
    }
    return false;
}


bool MainWindow::loadMIBFile()
{
       vector <string> mibStrings;
       QFile mib(pathToMIBFile);
       if (mib.open(QIODevice::ReadOnly)) {
       QTextStream in(&mib);
       while (!in.atEnd())
          mibStrings.push_back(in.readLine().toLocal8Bit().data());
       mib.close();
//       while ((!mibStrings.at(0)[0] == 'e') && (!mibStrings.at(0)[1] == 'l') && (!mibStrings.at(0)[2] == 'v') && (!mibStrings.at(0)[3] == 'a'))
//           mibStrings.erase(mibStrings.begin());
     bool enterpriseFound = false;
     map <string, OIDARR> groups;
     groups.insert(make_pair("enterprises",list<OIDTYPE>{1,3,6,1,4,1}));
     auto lineItEnd = mibStrings.end();
     for (auto lineIt = mibStrings.begin(); lineIt != lineItEnd; lineIt++) {
        if (!enterpriseFound) {
            enterpriseFound = searchEnterprises(lineIt,lineItEnd,groups);
        }
        else {
           if (searchGroupNames(*lineIt,groups)) {
                continue;
            }
            if (searchOid(lineIt,lineItEnd,groups,oidList,"OBJECT-TYPE")) {
                continue;
            }
         }
      }
     for (auto &x : oidList) {
        QString tmp;
        for (auto &y : x.oid) {
            tmp.append(QString::number(y)).append(".");
        }
        tmp.chop(1);
        OidColumn.append(x.name.c_str());
        qDebug() << x.name.c_str() << tmp;
        snmpCommands.insert(x.name.c_str(), tmp);
      }
    }

       return !snmpCommands.isEmpty();
    }

//    /*загрузка в память файла*/
//    QFile mibFile(pathToMIBFile);
//    QList <QByteArray> baList;//здесь хранится файл
//    if(!mibFile.open(QIODevice::ReadOnly))
//        return false;
//    else{
//    baList = mibFile.readAll().split('\n');
//    mibFile.close();
//    /*очистка от хлама сверху*/
//    while(!baList[0].startsWith("elva"))
//        baList.pop_front();

//    /*извлечения адреса "корня" и enterprize*/
//    QHash <QString,QByteArray> commAndAddr; //временное хранилище
//    QRegExp rx("(\(elva)\\w+)|(\\d{1,2})");
//    rx.indexIn(baList[1]);
//    QStringList str;
//    do{
//        int pos = 0;
//        while ((pos = rx.indexIn(baList[0], pos)) != -1) {
//            str << rx.cap(0) ;
//            pos += rx.matchedLength();
//        }

//        baList.pop_front();
//    }while(!baList[0].contains("--"));

//    for(int i = 1; i < str.size(); i = i + 2){
//        QString tempStr = "1.3.6.1.4.1." + str[0] + "." + str[i + 1];
//        commAndAddr.insert(str[i],tempStr.toLocal8Bit());
//    }
//    str.clear();
//    /*group start*/
//    /*извлечение адреса группы*/
//    do{
//        int pos = 0;
//        while ((pos = rx.indexIn(baList[0], pos)) != -1) {
//            str << rx.cap(0) ;
//            pos += rx.matchedLength();
//        }
//        baList.pop_front();
//    }while(!baList[0].contains("--"));

//    for(int i = 0; i < str.size(); i = i + 3){

//        QString root = str[i + 1];
//        QString command = str[i];
//        QString addr = "." + str[i + 2];
//        QByteArray fullAddress = commAndAddr.value(root);
//        fullAddress += addr;
//        commAndAddr.insert(command,fullAddress);
//    }

//    /*group end*/
//    /*поиск адресов комманд и самих команд*/

//    while(!(baList.size() < 6)){
//        str.clear();
//        do{
//            str << baList[0];
//            baList.pop_front();
//        }while(!baList[0].startsWith("elva") && !baList[0].startsWith("--") && !baList[0].startsWith("END"));

//        if(str.size() != 7)
//            continue;

//        QStringList rootAndAddres = str[5].remove(0,6).split(" ");
//        QString command = str[0].split(" ")[0];
//        QString addr = "." + rootAndAddres[1].split("}")[0];
//        QString description = str[4].split('"')[1];
//        QByteArray fullAddress = commAndAddr.value(rootAndAddres[0]);
//        fullAddress += addr;
//        snmpCommands.insert(command,fullAddress);
//        commandDescription.insert(command,description);
//    }
//    return true;
//    }


MainWindow::~MainWindow()
{
    for (unsigned short i = 0; i < numOfDlink; i++) {
        dlinkThread[i].quit();
        dlinkThread[i].wait();
    }
    for (int i = 0; i < 2; i++) {
        for (int k = 0; k < radioThread[i].size(); k++) {
            radioThread[i][k]->quit();
            radioThread[i][k]->wait();
        }
    }
}
