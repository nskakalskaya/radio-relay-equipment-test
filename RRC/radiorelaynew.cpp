#include "radiorelaynew.h"
#include <QMessageBox>
#include <QFile>

RadioRelay::RadioRelay(const unsigned int &numPortOnDlink, const unsigned int &numRadio, QObject *parent) :
    SnmpSimple(parent),
    maxTemp(0),
    isNeedHeater(false),
    updateHeatTimer(0),
    heatTimer(0),
    coolTimer(0)
{
    numPortOnDlink2 = numPortOnDlink;
    numRadio2 = numRadio;
    rrcInfo.numPortOnDlink = numPortOnDlink;
    rrcInfo.numRadio = numRadio;
    port = 161;
    //interval = 100;
    readCommunityName = "public";
    writeCommunityName = "public";
    /*список команд, которые исползуются для  общения с релейкой*/
    for(int i = 0; i < NUMOFCOMMANDSID; i++)
        commandsId[i] = "elvaIpID" + QString::number(i + 1);
}

void RadioRelay::connectToSnmp(const QString &ipaddr)
{
    if (updateHeatTimer)
       delete updateHeatTimer;

    if (heatTimer)
        delete heatTimer;

    if (coolTimer)
        delete coolTimer;

    updateHeatTimer = new QTimer(this);
    heatTimer = new QTimer(this);
    coolTimer = new QTimer(this);

    updateHeatTimer->setInterval(1000 * 60);//одна минута
    heatTimer->setInterval(heatInterval);
    coolTimer->setInterval(coolInterval);
    heatTimer->setSingleShot(true);
    coolTimer->setSingleShot(true);

    connect(updateHeatTimer,&QTimer::timeout,this,&RadioRelay::turnOnHeater);
    connect(heatTimer,&QTimer::timeout,updateHeatTimer,&QTimer::stop);//когда вышло время нагрева, выключаем ежеминутное включение обогрева
    connect(heatTimer,SIGNAL(timeout()),coolTimer,SLOT(start()));//и после этого включаем таймер охлаждения
    connect(heatTimer,&QTimer::timeout,this,&RadioRelay::turnOffHeater);//и принудительно выключаем нагрев
    connect(coolTimer,SIGNAL(timeout()),updateHeatTimer,SLOT(start()));
    connect(coolTimer,SIGNAL(timeout()),heatTimer,SLOT(start()));
    connect(coolTimer,&QTimer::timeout,this,&RadioRelay::turnOnHeater);//после остывания включаем, нагрев
    connect(this,&SnmpSimple::error,this,&RadioRelay::slotError);

    emit stopTimer();
    address = ipaddr.toLocal8Bit().data();
    address.set_port(port);

    if (initializeSnmp()){
        if (getIDInformation())
        {
            emit connected();
            emit startTimer();
        }
    }


}

void RadioRelay::getStat()
{
    QStringList lst, res;
    if (rrcInfo.gigabyteModel) {
        lst << "1.3.6.1.4.1.47.1.2.2" //elvaIpTS1
            << "1.3.6.1.4.1.47.1.2.3" //elvaIpTS2
            << "1.3.6.1.4.1.47.1.3.3" //elvaIpUAG  - AGC
            << "1.3.6.1.4.1.47.1.3.4" //elvaIpATT  - RSL
            << "1.3.6.1.4.1.47.1.4.3" //elvaIpUN9  - -9V
            << "1.3.6.1.4.1.47.1.4.4" //elvaIpUP5 - +5V
            << "1.3.6.1.4.1.47.1.4.5" //elvaIpU5S - 5stanby
            << "1.3.6.1.4.1.47.1.4.6" //elvaIpUII - CONS
            << "1.3.6.1.4.1.47.1.5.1" //elvaIpOnP - Radio
            << "1.3.6.1.4.1.47.1.5.3" //elvaIpOn1 - Heater
            << "1.3.6.1.4.1.47.1.6.1" //elvaIpERX
            << "1.3.6.1.4.1.47.1.6.2" //elvaIpETX
            << "1.3.6.1.4.1.47.1.6.5" //elvaIpSTA - SNR
            << "1.3.6.1.4.1.47.1.6.6" //elvaIpST2 - Downlink
            << "1.3.6.1.4.1.47.1.6.7" //elvaIpST3 - Uplink
            << "1.3.6.1.4.1.47.1.6.18"; //elvaIpCRC
         //   << "1.3.6.1.4.1.47.1.6.4"; //elvaIpRSF - FecFailTotal

//            << snmpCommands.value("elvaIpERX")
//            << snmpCommands.value("elvaIpETX")
//            << snmpCommands.value("elvaIpRSF")//4 //error

//            << snmpCommands.value("elvaIpURE")
//            << snmpCommands.value("elvaIpUTR")
//            << snmpCommands.value("elvaIpUAG")
//            << snmpCommands.value("elvaIpATT")//8

//            << snmpCommands.value("elvaIpU32")
//            << snmpCommands.value("elvaIpU10")
//            << snmpCommands.value("elvaIpUN12")
//            << snmpCommands.value("elvaIpUP5")
//            << snmpCommands.value("elvaIpU5S")//13
//            << snmpCommands.value("elvaIpUII")
//            << snmpCommands.value("elvaIpSTA")
//            << snmpCommands.value("elvaIpON1")
//            << snmpCommands.value("elvaIpONP");//17

//        lst << "1.3.6.1.4.1.47.1.2.2"
//            << "1.3.6.1.4.1.47.1.2.3"

//            << "1.3.6.1.4.1.47.1.6.1"
//            << "1.3.6.1.4.1.47.1.6.2"
//            << "1.3.6.1.4.1.47.1.6.4" //4 //error

//            << "1.3.6.1.4.1.47.1.6.4"
//            << "1.3.6.1.4.1.47.1.3.2"
//            << "1.3.6.1.4.1.47.1.3.3"
//            << "1.3.6.1.4.1.47.1.3.3"

//            << "1.3.6.1.4.1.47.1.3.3"
//            << "1.3.6.1.4.1.47.1.3.3"
//            << "1.3.6.1.4.1.47.1.3.3"
//            << "1.3.6.1.4.1.47.1.4.4"
//            << "1.3.6.1.4.1.47.1.4.5"//13
//            << "1.3.6.1.4.1.47.1.4.6"
//            << "1.3.6.1.4.1.47.1.6.5"
//            << "1.3.6.1.4.1.47.1.5.3"
//            << "1.3.6.1.4.1.47.1.5.1";//17

        res = getSnmp(lst);
        if (res.size() != lst.size())
            return;

        rrcInfo.tempSensor1 = QString::number(res[0].toDouble() / 100.0);
        rrcInfo.tempSensor2 = QString::number(res[1].toDouble() / 100.0);
        rrcInfo.UAG = QString::number(res[2].toDouble() / 100.0);
        rrcInfo.ATT = QString::number(res[3].toDouble());
        rrcInfo.UMIN12V = QString::number(res[4].toDouble() / 100.0);
        rrcInfo.U5V = QString::number(res[5].toDouble() / 100.0);
        rrcInfo.U5VSB = QString::number(res[6].toDouble() / 100.0);
        rrcInfo.consumption = QString::number(res[7].toDouble() / 100.0);
        rrcInfo.radioStatus = res[8].toInt();
        rrcInfo.heaterStatus = res[9].toInt();
        rrcInfo.RXPacketsPerSec = res[10];
        rrcInfo.TXPacketsPerSec = res[11];
        rrcInfo.URE = QString::number(res[12].toDouble() / 100.0);
        rrcInfo.downlink = res[13];
        rrcInfo.uplink = res[14];
        rrcInfo.MSE = QString::number(res[15].toDouble());
        rrcInfo.time = QTime::currentTime().toString("hh:mm:ss");







//        rrcInfo.uplink = "10000";
//        rrcInfo.downlink = "10000";
//        rrcInfo.RXPacketsPerSec = res[2];
//        rrcInfo.TXPacketsPerSec = res[3];
//        rrcInfo.fecFailTotal = res[4];
//        rrcInfo.URE = QString::number(res[5].toDouble() / 1000.0);
//        rrcInfo.UTR = QString::number(res[6].toDouble() / 1000.0);
//        rrcInfo.UAG = QString::number(res[7].toDouble() / 1000.0);
//        rrcInfo.ATT = QString::number(res[8].toDouble() / 10.0);
//        rrcInfo.U24V = QString::number(res[9].toDouble() / 1000.0);
//        rrcInfo.UPLUS10V = QString::number(res[10].toDouble() / 1000.0);
//        rrcInfo.UMIN12V = QString::number(res[11].toDouble() / -1000.0);
//        rrcInfo.U5V = QString::number(res[12].toDouble() / 1000.0);
//        rrcInfo.U5VSB = QString::number(res[13].toDouble() / 1000.0);
//        rrcInfo.consumption = QString::number(res[14].toDouble() / 10.0);

//        rrcInfo.MSE = res[15];
//        rrcInfo.heaterStatus = res[16].toInt();
//        rrcInfo.radioStatus = res[17].toInt();

    }
    else {
        lst << "1.3.6.1.4.1.47.1.2.2" //elvaIpTS1
            << "1.3.6.1.4.1.47.1.2.3" //elvaIpTS2
            << "1.3.6.1.4.1.47.1.3.3" //elvaIpUAG  - AGC
            << "1.3.6.1.4.1.47.1.3.4" //elvaIpATT  - RSL
            << "1.3.6.1.4.1.47.1.4.3" //elvaIpUN9  - -9V
            << "1.3.6.1.4.1.47.1.4.4" //elvaIpUP5 - +5V
            << "1.3.6.1.4.1.47.1.4.5" //elvaIpU5S - 5stanby
            << "1.3.6.1.4.1.47.1.4.6" //elvaIpUII - CONS
            << "1.3.6.1.4.1.47.1.5.1" //elvaIpOnP - Radio
            << "1.3.6.1.4.1.47.1.5.3" //elvaIpOn1 - Heater
            << "1.3.6.1.4.1.47.1.6.1" //elvaIpERX
            << "1.3.6.1.4.1.47.1.6.2" //elvaIpETX
            << "1.3.6.1.4.1.47.1.6.5" //elvaIpSTA - SNR
            << "1.3.6.1.4.1.47.1.6.6" //elvaIpST2 - Downlink
            << "1.3.6.1.4.1.47.1.6.7" //elvaIpST3 - Uplink
            << "1.3.6.1.4.1.47.1.6.18"; //elvaIpCRC
        res = getSnmp(lst);
        if (res.size() != lst.size())
            return;
        rrcInfo.tempSensor1 = QString::number(res[0].toDouble() / 100.0);
        rrcInfo.tempSensor2 = QString::number(res[1].toDouble() / 100.0);
        rrcInfo.UAG = QString::number(res[2].toDouble() / 100.0);
        rrcInfo.ATT = QString::number(res[3].toDouble());
        rrcInfo.UMIN12V = QString::number(res[4].toDouble() / 100.0);
        rrcInfo.U5V = QString::number(res[5].toDouble() / 100.0);
        rrcInfo.U5VSB = QString::number(res[6].toDouble() / 100.0);
        rrcInfo.consumption = QString::number(res[7].toDouble() / 100.0);
        rrcInfo.radioStatus = res[8].toInt();
        rrcInfo.heaterStatus = res[9].toInt();
        rrcInfo.RXPacketsPerSec = res[10];
        rrcInfo.TXPacketsPerSec = res[11];
        rrcInfo.URE = QString::number(res[12].toDouble() / 100.0);
        rrcInfo.downlink = res[13];
        rrcInfo.uplink = res[14];
        rrcInfo.MSE = QString::number(res[15].toDouble());
        rrcInfo.time = QTime::currentTime().toString("hh:mm:ss");

        //        rrcInfo.tempSensor1 = QString::number(res[0].toDouble() / 100.0);
//        rrcInfo.tempSensor2 = QString::number(res[1].toDouble() / 100.0);
//            rrcInfo.tempSensor3 = QString::number(res[2].toDouble() / 100.0);

//        rrcInfo.uplink = res[3];
//        rrcInfo.downlink = res[4];

//        rrcInfo.RXPacketsPerSec = res[5];
//        rrcInfo.TXPacketsPerSec = res[6];
//        rrcInfo.fecFailTotal = res[7];
//        rrcInfo.dropRXpackets = res[8];
//        rrcInfo.dropTXpackets = res[9];

//        rrcInfo.offsetHz = QString::number(res[10].toDouble() / 1000000.0);

//        rrcInfo.URE = QString::number(res[11].toDouble() / 1000.0);
//        rrcInfo.UTR = QString::number(res[12].toDouble() / 1000.0);
//        rrcInfo.UAG = QString::number(res[13].toDouble() / 1000.0);
//        rrcInfo.ATT = QString::number(res[14].toDouble() / 10.0);
//        rrcInfo.U24V = QString::number(res[15].toDouble() / 1000.0);
//        rrcInfo.UPLUS10V = QString::number(res[16].toDouble() / 1000.0);
//        rrcInfo.UMIN12V = QString::number(res[17].toDouble() / -1000.0);
//        rrcInfo.U5V = QString::number(res[18].toDouble() / 1000.0);
//        rrcInfo.U5VSB = QString::number(res[19].toDouble() / 1000.0);
//        rrcInfo.consumption = QString::number(res[20].toDouble() / 10.0);

//        rrcInfo.MSE = QString::number(res[21].toDouble() / 100.0);
//        rrcInfo.heaterStatus = res[22].toInt();
//        rrcInfo.radioStatus = res[23].toInt();
//        rrcInfo.time = QTime::currentTime().toString("hh:mm:ss");
    }

    emit haveRRCInfo(rrcInfo);

    /*управление нагревом и проверка на перегрев*/

    if(isNeedHeater){
        if(!heatTimer->isActive()) // если включен режим охлаждения или не включен режим нагрева, то выходим
            return;

        if(rrcInfo.tempSensor1.toFloat()> maxTemp){//если перегрев
            if(updateHeatTimer->isActive()){
                turnOffHeater();
                updateHeatTimer->stop();
            }
        }
        else //если перегрева нет, но были выключен таймер в результате перегрева, то включаем
            if(heatTimer->isActive() && !updateHeatTimer->isActive()){
                //turnOnHeater();
                updateHeatTimer->start();
            }
    }
    else{
        if(coolTimer->isActive())
            coolTimer->stop();
        if(updateHeatTimer->isActive())
            updateHeatTimer->stop();
        if(heatTimer->isActive())
            heatTimer->stop();
    }
}

void RadioRelay::startHeat()
{    
    if(!connectStat)
        return;

    if(heatTimer->interval() > 0 && coolTimer->interval() > 0 && maxTemp > 0 && isNeedHeater){
        if(rrcInfo.tempSensor1.toFloat() < this->maxTemp){
            turnOnHeater();
            updateHeatTimer->start();
        }
        heatTimer->start();
    }
}

void RadioRelay::stopHeat()
{
    if(!isConnected())
        return;

    coolTimer->stop();
    updateHeatTimer->stop();
    heatTimer->stop();
    turnOffHeater();
}

void RadioRelay::setHeater(bool heater)
{
    this->isNeedHeater = heater;
    if(isNeedHeater){
        if(rrcInfo.tempSensor1.toFloat() < maxTemp){
            turnOnHeater();
        }
    }
}

void RadioRelay::setMaxTemp(const float& maxTemp)
{
    this->maxTemp = maxTemp;
}

void RadioRelay::setHeatTime(const int &mSec)
{
    heatInterval = mSec;
    if(heatTimer)
        heatTimer->setInterval(heatInterval);
}

void RadioRelay::setCoolTime(const int& mSec)
{
    coolInterval = mSec;
    if(coolTimer)
        coolTimer->setInterval(coolInterval);
}

void RadioRelay::writeSettings(const QHash <QString,QString> &snmpCommands, const QHash<QString, QString> &snmpDescription)
{
    this->snmpCommands = snmpCommands;
    this->commandDescription = snmpDescription;
}

bool RadioRelay::getIDInformation()
{
    QString result = "";
    QString model, serialNumber;

    model = getSnmp("1.3.6.1.4.1.47.1.1.1");
    serialNumber = getSnmp("1.3.6.1.4.1.47.1.1.2");

    result = model + "\n" + serialNumber;


//    result += getSnmp(snmpCommands.value(commandsId[0])).replace("\n","") + "\n";


    if(!connectStat)//нет соединения
        return connectStat;

//    serialNumber = getSnmp(snmpCommands.value(commandsId[1])).replace("\n","") + "\n";
//    result += serialNumber;

//    if(serialNumber.contains("GPM"))
//        rrcInfo.gigabyteModel = true;
//    else
//        rrcInfo.gigabyteModel = false;
//    emit isGigabyte(rrcInfo.gigabyteModel);

//    for(int i = 2; i < NUMOFCOMMANDSID; i++)
//        result += getSnmp(snmpCommands.value(commandsId[i])).replace("\n","") + "\n";


    emit sendSignal(serialNumber.split(':').last());
    emit haveID(result);

//    model = model.split(" ").last().replace("/","-").replace("\n","");
//    serialNumber = serialNumber.split(": ").last().replace("\n","");

//    int numRadio = rrcInfo.numRadio;
//    emit haveModel(numRadio,model);
//    emit haveSerialNumber(numRadio,serialNumber);
    return connectStat;
}

void RadioRelay::turnOnHeater()
{
    setSnmp(snmpCommands.value("elvaIpON1"),1);
}

void RadioRelay::turnOffHeater()
{
    setSnmp(snmpCommands.value("elvaIpON1"),0);
}

RadioRelay::~RadioRelay()
{
}

void RadioRelay::slotError(QString error)
{
    emit errorSignal(error, numRadio2, numPortOnDlink2);
}
