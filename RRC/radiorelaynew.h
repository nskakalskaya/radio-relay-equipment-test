#ifndef RADIORELAY_H
#define RADIORELAY_H

#include <QVector>
#include <QStringList>
#include <QHash>
#include <QTime>
#include <QColor>
#include <QDebug>

#include "../snmpsimple.h"

#define NUMOFCOMMANDSID 8

struct RRCInfo {
    unsigned int numPortOnDlink, numRadio;
    QString tempSensor1, tempSensor2;
    QString ATT;
    QString uplink, downlink;
    QString RXPacketsPerSec, TXPacketsPerSec;
    //QString totalRXPacket,totalTXPacket;
 //  QString fecFailTotal;
   // QString dropRXpackets, dropTXpackets;
  //  QString offsetHz;
    QString MSE;
    QString UAG,UMIN12V,U5V,U5VSB,URE;
    QString consumption;
    QString time;
    bool gigabyteModel;
    bool radioStatus;
    bool heaterStatus;
};

class RadioRelay : public SnmpSimple
{
    Q_OBJECT
    unsigned int numPortOnDlink2,numRadio2;
    RRCInfo rrcInfo;
    /*параметры для радиорелейки*/

    QVector <double> tempVolt, temperature;

    QHash <QString,QString> snmpCommands; //для хранения oid команд, первое значение имя команды(как и в остальных мапах)
    QHash <QString,QString> commandDescription; //расшифровка команд

    //bool gigabyteModel;//состояние радио, тип модели, состояние соединения
    bool isNeedHeater;//надо ли греть
    QTimer* updateHeatTimer;//необходимо раз в минуту включать нагрев
    QTimer* heatTimer;//время нагрева
    QTimer* coolTimer;//время остывания
    float maxTemp;//максимальная температура
    quint64 heatInterval, coolInterval;

    /*секция команд*/
    QString commandsId[NUMOFCOMMANDSID];    

    bool getIDInformation();

public:
    RadioRelay(const unsigned int &numPortOnDlink, const unsigned int &numRadio,QObject *parent = 0);
    ~RadioRelay();       

    void writeSettings(const QHash<QString, QString> &snmpCommands, const QHash <QString,QString> &snmpDescription);

    void setMaxTemp(const float& maxTemp);
    void setHeatTime(const int& mSec);
    void setCoolTime(const int &mSec);
    void setHeater(bool heater);//устанавливааем занчение греть или нет нужен ли нагрев или нет

    int getNumPort(){return numPortOnDlink2;}
    int getNumRadio(){return numRadio2;}

    QString getIpTS1Oid(QString name);




signals:
    void haveID(QString id);
    void haveModel(int numRadio,const QString& model);
    void haveSerialNumber(int numRadio,const QString& model);
    void haveRRCInfo(RRCInfo rrcInfo);
    //void turnOnRadio(bool radioSwitch);//в случае самовключения
    void isGigabyte(bool state);
    void errorSignal(QString error,int numRadio, int numPortOnDlink);
    void sendSignal(QString model);

public slots:
    void getStat();
    void connectToSnmp(const QString &ipaddr);
    void turnOnHeater();
    void turnOffHeater();
    void startHeat();
    void stopHeat();
    void slotError(QString error);
};

#endif // RADIORELAY_H
