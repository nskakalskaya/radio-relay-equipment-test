#include "config.h"

Config::Config(QObject *parent) : QObject(parent),
  notConnectedColor(QColor("orange")),
  connectedColor(QColor("blue"))
{    
    cnf = new QSettings("setting.ini", QSettings::IniFormat);
    QFileInfo fi("setting.ini");
    if(fi.exists())
    {
        mibPath = cnf->value("mib_path").toString();        
        dlinkIp[0] = cnf->value("ip_dlink_0").toString();
        dlinkIp[1] = cnf->value("ip_dlink_1").toString();
        generatorPort = cnf->value("generator_port").toString();
        listenPort = cnf->value("listen_port").toString().split("/");
    }
    else
    {
        mibPath = "rrc.mib";
        dlinkIp[0] = "192.168.127.95";
        dlinkIp[1] = "192.168.127.96";
        generatorPort = "23";
        listenPort << "3" << "4" << "5" << "6" << "7" << "8";
    }

    perBorder.yellowBorder = pow(10,-7);
    perBorder.redBorder = pow(10,-6);

    lostBorder.yellowBorder = pow(10,-7);
    lostBorder.redBorder = pow(10,-6);

    /*yellowBorderStepen = cnf->value("yellow_border").toDouble();
    redBorderStepen = cnf->value("red_border").toDouble();
    yellowBorder = pow(10,yellowBorderStepen);
    redBorder = pow(10,redBorderStepen);*/
}

QString Config::getMibPath()
{
    return mibPath;
}

Border Config::getPerBorder()
{
    return perBorder;
}

Border Config::getLostBorder()
{
    return lostBorder;
}

QString Config::getDlinkIp0()
{
    return dlinkIp[0];
}

QString Config::getDlinkIp1()
{
    return dlinkIp[1];
}

QString Config::getGeneratorPort()
{
    return generatorPort;
}

QStringList Config::getListenPort()
{
    return listenPort;
}

QColor Config::getNotConnectedColor()
{
    return notConnectedColor;
}

QColor Config::getConnectedColor()
{
    return connectedColor;
}

void Config::setMibPath(QString mibPath)
{
    this->mibPath = mibPath;
}

void Config::setDlinkIp0(const QString &dlinkIp)
{
    this->dlinkIp[0] = dlinkIp;
}

void Config::setDlinkIp1(const QString &dlinkIp)
{
    this->dlinkIp[1] = dlinkIp;
}

void Config::saveConfig()
{
    cnf->setValue("mib_path", mibPath);
    //cnf->setValue("yellow_border", yellowBorderStepen);
    //cnf->setValue("red_border", redBorderStepen);
    cnf->setValue("ip_dlink_0", dlinkIp[0]);
    cnf->setValue("ip_dlink_1", dlinkIp[1]);
    cnf->setValue("generator_port",generatorPort);
    QString str;
    if(listenPort.size() > 0){
        str = listenPort[0];
        for(int i = 1; i < listenPort.size(); i++){
            str += "/";
            str += listenPort[i];
        }
    }

    cnf->setValue("listen_port",str);
}

Config::~Config()
{
    saveConfig();
    delete cnf;
}
