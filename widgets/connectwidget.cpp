#include "connectwidget.h"

ConnectWidget::ConnectWidget(const unsigned int &numChannel, const unsigned int &numRadio, Config &cnf, QWidget *parent) : QWidget(parent),
    numChannel(numChannel),
    numRadio(numRadio),
    ip(new QLineEdit("192.168.127.",this)),
    radTextEdit(new QTextEdit(this)),
    connectToRadioButton(new QPushButton("Connect",this)),
    cnf(cnf)
{

    ip->setInputMask("000.000.000.000");
    ip->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    radTextEdit->setReadOnly(true);
    radTextEdit->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Fixed);

    QGroupBox *radarBox = new QGroupBox("Radiorelay " + QString::number(numRadio),this);
    QHBoxLayout *inputLayout = new QHBoxLayout;
    inputLayout->addStretch(1);
    inputLayout->addWidget(new QLabel("IP - address",this));
    if ((numChannel == 0) && (numRadio == 0))
        ip->setText("192.168.127.101");
    else if ((numChannel == 0) && (numRadio == 1))
        ip->setText("192.168.127.102");
    else if ((numChannel == 1) && (numRadio == 0))
        ip->setText("192.168.127.103");
    else if ((numChannel == 1) && (numRadio == 1))
        ip->setText("192.168.127.104");
    else if ((numChannel == 2) && (numRadio == 0))
        ip->setText("192.168.127.105");
    else if ((numChannel == 2) && (numRadio ==1))
        ip->setText("192.168.127.106");
    else if ((numChannel == 3) && (numRadio ==0))
        ip->setText("192.168.127.107");
    else if ((numChannel == 3) && (numRadio == 1))
        ip->setText("192.168.127.108");
    else if ((numChannel == 4) && (numRadio == 0))
        ip->setText("192.168.127.109");
    else if ((numChannel == 4) && (numRadio == 1))
        ip->setText("192.168.127.110");
    else if ((numChannel == 5) && (numRadio == 0))
        ip->setText("192.168.127.111");
    else if ((numChannel == 5) && (numRadio == 1))
        ip->setText("192.168.127.112");

    inputLayout->addWidget(ip);
    inputLayout->addWidget(connectToRadioButton);
    inputLayout->addStretch(1);

    QVBoxLayout *radarGridLayout = new QVBoxLayout;
    radarGridLayout->addLayout(inputLayout);
    radarGridLayout->addWidget(radTextEdit);

    radarBox->setLayout(radarGridLayout);
    radarBox->setAlignment(Qt::AlignCenter);
    connectToRadioButton->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);


    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(radarBox);

    QPalette p = connectToRadioButton->palette();
    p.setColor(QPalette::Button,this->cnf.getNotConnectedColor());
    connectToRadioButton->setPalette(p);
    connectToRadioButton->setAutoFillBackground(true);

    setLayout(mainLayout);

    connect(connectToRadioButton, &QPushButton::clicked,this,&ConnectWidget::pressedConnectToRadioRelay);
}

void ConnectWidget::pressedConnectToRadioRelay()
{
    emit connectToRadioRelay(ip->text());
}

void ConnectWidget::fillTextEditRadioRelay(const QString &str)
{
    radTextEdit->clear();
    radTextEdit->append(str);
}

void ConnectWidget::connected()
{
    disconnect(connectToRadioButton, &QPushButton::clicked,this,&ConnectWidget::pressedConnectToRadioRelay);
    connect(connectToRadioButton, &QPushButton::clicked,this,&ConnectWidget::disconnectFromRadioRelay);
    connectToRadioButton->setText("Disconnect");
    QPalette p = connectToRadioButton->palette();
    p.setColor(QPalette::Button,cnf.getConnectedColor());
    connectToRadioButton->setPalette(p);
}

void ConnectWidget::disconnected()
{
    disconnect(connectToRadioButton, &QPushButton::clicked,this,&ConnectWidget::disconnectFromRadioRelay);
    connect(connectToRadioButton, &QPushButton::clicked,this,&ConnectWidget::pressedConnectToRadioRelay);
    connectToRadioButton->setText("Connect");
    QPalette p = connectToRadioButton->palette();
    p.setColor(QPalette::Button,cnf.getNotConnectedColor());
    connectToRadioButton->setPalette(p);
    radTextEdit->clear();
}

void ConnectWidget::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return)
        connectToRadioButton->clicked();
}

ConnectWidget::~ConnectWidget()
{

}

