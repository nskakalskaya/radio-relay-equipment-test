#ifndef ATTCONNECTWIDGET_H
#define ATTCONNECTWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QTextEdit>
#include <QComboBox>
#include <QGroupBox>
#include <QCheckBox>
#include <QPushButton>
#include <QLayout>
#include <QMap>
#include "config.h"
#include "windows.h"
#include "SLABCP2110.h"
#include "paramsWindowWidgets/attparamwidget.h"

class AttConnectWidget : public QWidget
{
    Q_OBJECT
    QPushButton *connectToAttButton;
    Config &cnf;    
    AttParamWidget *attParamWidget;
    QMap<int, QString >devList;
    int attNumber;
    bool ifConnected, ifTurnedOn;
    QString serial;
    QString getSerialString(int devIndex);
    static const WORD VID = 0x10C4;
    static const WORD PID = 0xEA80;

public:
    explicit AttConnectWidget(Config &cnf, int attNum, QWidget *parent = 0);
    QCheckBox *turnAttBox;
    ~AttConnectWidget();

signals:
    void connectToAttenuator(int deviceIndex);
    void disconnectFromAttenuator();
    void startEscalating();
    void stopEscalating();
    void sendMinMaxValues();
    void sendMinDbValue(int min);
    void sendMaxDbValue(int max);

public slots:
    void pressedConToAtt(int deviceIndex);
    void fillTextEditAtt(const QString &str);
    void connected();
    void disconnected();
    void turnOnAtt(bool state, int deviceIndex);

};

#endif // ATTCONNECTWIDGET_H
