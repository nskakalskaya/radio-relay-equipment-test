#include "dlinkwidget.h"

DlinkWidget::DlinkWidget(const unsigned int &numDlink,Config &cnf,QWidget *parent) : QWidget(parent),
    cnf(cnf)
{
    ipDlink = new QLineEdit(this);
  //  ipDlink->setInputMask("000.000.000.000");
    QGroupBox *dlinkGroupBox = new QGroupBox("dlink 3120 " + QString::number(numDlink),this);
    connectDlinkBut = new QPushButton("Connect",this);
    QHBoxLayout *dlinkLayout = new QHBoxLayout;
    QHBoxLayout *dlinkConnectLayout = new QHBoxLayout;
    dlinkConnectLayout->addStretch(1);
    dlinkConnectLayout->addWidget(new QLabel("IP - address",this));
    if (numDlink == 0)
        ipDlink->setText("192.168.127.95");
    else if (numDlink == 1)
        ipDlink->setText("192.168.127.96");
    dlinkConnectLayout->addWidget(ipDlink);
    dlinkConnectLayout->addWidget(connectDlinkBut);
    dlinkConnectLayout->addStretch(1);
    dlinkGroupBox->setLayout(dlinkConnectLayout);
    dlinkGroupBox->setAlignment(Qt::AlignCenter);
    dlinkLayout->addWidget(dlinkGroupBox);
    QPalette p = connectDlinkBut->palette();
    p.setColor(QPalette::Button,this->cnf.getNotConnectedColor());
    connectDlinkBut->setPalette(p);
    connectDlinkBut->setAutoFillBackground(true);
    setLayout(dlinkLayout);
    connect(connectDlinkBut,&QPushButton::clicked,this,&DlinkWidget::pressedConToDlink);
    connect(ipDlink,&QLineEdit::textChanged,this,&DlinkWidget::textChanged);

}

void DlinkWidget::connected()
{
    connectDlinkBut->setText("Disconnect");
    disconnect(connectDlinkBut,&QPushButton::clicked,this,&DlinkWidget::pressedConToDlink);
    connect(connectDlinkBut,&QPushButton::clicked,this,&DlinkWidget::disconnectFromDlink);

    QPalette p = connectDlinkBut->palette();
    p.setColor(QPalette::Button,cnf.getConnectedColor());
    connectDlinkBut->setPalette(p);
}

void DlinkWidget::disconnected()
{
    connectDlinkBut->setText("Connect");
    disconnect(connectDlinkBut,&QPushButton::clicked,this,&DlinkWidget::disconnectFromDlink);
    connect(connectDlinkBut,&QPushButton::clicked,this,&DlinkWidget::pressedConToDlink);

    QPalette p = connectDlinkBut->palette();
    p.setColor(QPalette::Button,cnf.getNotConnectedColor());
    connectDlinkBut->setPalette(p);
}

void DlinkWidget::pressedConToDlink()
{
    emit connectToDlink(ipDlink->text());
}

void DlinkWidget::setText(const QString &str)
{
    ipDlink->setText(str);
}

void DlinkWidget::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return)
        connectDlinkBut->clicked();
}

DlinkWidget::~DlinkWidget()
{

}

