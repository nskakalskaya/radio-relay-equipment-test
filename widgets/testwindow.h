#ifndef TESTWINDOW_H
#define TESTWINDOW_H

#include <QWidget>
#include <QPushButton>
#include <QGroupBox>
#include <QLineEdit>
#include <QLabel>
#include <QLayout>
#include <QTimer>
#include <QThread>
#include <QtAlgorithms>
#include <QMetaobject>
#include "test.h"
#include "attenuator/attenuatorppc.h"
#include "config.h"
#include "paramsWindowWidgets/attparamwidget.h"
#include "paramsWindowWidgets/heatparamWidget.h"
#include "testWindowWidgets/resultwidget.h"
#include "testWindowWidgets/testparamwidget.h"

//TODO
/* по нажатию кнопки старт создаются коннекты:
 * коннект сигнал с релейки с собранной информацией слот обработка информации+
 * сброс статистики на длинке+
 * запускается таймер timeOnePass+
 * запускается основной таймер длительности теста+
 */

/* по нажатию кнопки стоп //stopRRC
 * удалается соотвествующий коннект +
 * отключается релейка+
 * записывается в файл информация с длинка+
 */

/* таймер timeOnePass //timeOnePassTimeOut
 * остановка 23 порта+
 * записывает информацию с длинка +
 * сбрасывает статистику портов+
 * проверяет подключена ли релейка к этому порту+
 * если подключена создает коннект для этой релейки+
 * включает 23 порт+
 */

/* слот обработки информации
 * принимает решение о цвете индикации релейки(перенести в класс релейки)
 * записывает информацию в файл+
 * если релейка красная, принимает решение по отключению релейки
 * если релейка желтая, запускает таймер продолжительностью сутки
 */

/* суточный таймер
 * по его истечению принимается решение по отключению или продолжению работы релейки
 */

/* timeFullPass запускает timeOnePassTimeOut
 * выключает 23 порт+
 * записывает информацию с длинка+
 * закрывает фаил
 * сбрасывает статистику портов+
 * включает 23 порт+
 */


class QCLabel: public QLabel
{
    Q_OBJECT
public:
    explicit QCLabel(QColor color,QWidget *parent = 0);

public slots:
    void setBackgroundColor(QColor color){setPalette(QPalette(color));setAutoFillBackground(true);}

};

class TestWindow : public QWidget
{
    Q_OBJECT
    QFile * logFile;
    Config &cnf;
    QVector <RadioRelay *> radioRelay[2];
    QVector <ChannelProcessing *> channelProcessing;
    QVector <QThread *> channelProcessingThread;
    QVector <AttenuatorPPC *> attenuatorPPC;
    QVector <Dlink3120new * > dlink3120;    
    QVector <QCLabel *> relayStatusLabel;//цветовая индикация радиорелеек
    QVector <QPushButton *> relayStopButton;
    QStringList channelList;
    QPushButton *startTestBut;

    QVector <QLabel *> fullPassTimerLabel;

    void writeToLog(QString str);

    Test *test;
    void setEnabledStopButton(const int &numButton,const bool &state);

public:
    explicit TestWindow(QStringList &channelList,
                        QVector<Dlink3120new * > &dlink3120,
                        QVector<RadioRelay *> (&radioRelay)[2],
                        QVector <AttenuatorPPC *> &attenuatorPPC,
                        Config &cnf,
                        QWidget *parent = 0);

    ~TestWindow();

signals:
    void channelActive(const int& numChannel,const bool& state);///
    void testActive(const bool& state);

public slots:
    void setFullPassTimer(const QStringList str);//оставшееся время до окончания теста

};

#endif // TESTWINDOW_H
