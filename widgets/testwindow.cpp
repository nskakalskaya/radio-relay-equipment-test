#include "testwindow.h"
#include <QThread>

///TODO
/// подправить, тест не запускался без подключенных релеек+
/// включение нагревателей+но лучше переделать
/// блокировка окна подключения(выборочная)+
/// разблокировка окна подключения(выборочная)+
/// таймер нагрева и охлаждения
/// при обрыве связи, выключать канал из теста+

QCLabel::QCLabel(QColor color, QWidget *parent): QLabel(parent)
{
    setPalette(QPalette(color));
    setAutoFillBackground(true);
}

TestWindow::TestWindow(QStringList &channelList, QVector<Dlink3120new *> &dlink3120, QVector<RadioRelay *> (&radioRelay)[2], QVector<AttenuatorPPC *> &attenuatorPPC, Config &cnf, QWidget *parent) : QWidget(parent),
    cnf(cnf),
    dlink3120(dlink3120),
    radioRelay(radioRelay),
    attenuatorPPC(attenuatorPPC),
    channelList(channelList)
{

    TestParamWidget *testParamWidget = new TestParamWidget(this);
    QVector <ResultWidget *> resultWidget;

    qRegisterMetaType< DlinkInfo >("DlinkInfo");
    qRegisterMetaType< RRCInfo >("RRCInfo");
    qRegisterMetaType< QVector<DlinkInfo> >("QVector<DlinkInfo>");
    qRegisterMetaType< QVector<QString> >("QVector<QString>");

    startTestBut = new QPushButton("Start test",this);

    QGridLayout *testResultLayout = new QGridLayout;
    QSignalMapper *stopButtonMapper = new QSignalMapper(this);

    for(int i = 0; i < channelList.size(); i++){
        channelProcessing.push_back(new ChannelProcessing(i,channelList[i].toUInt(),this->cnf));
        channelProcessingThread.push_back(new QThread(this));
        channelProcessing[i]->moveToThread(channelProcessingThread[i]);
        /*цветовая индикация для канала*/
        relayStatusLabel.push_back(new QCLabel(QColor("green"),this));
        relayStatusLabel[i]->setDisabled(true);
        /*кнопка стоп для канала*/
        relayStopButton.push_back(new QPushButton("Stop",this));
        relayStopButton[i]->setDisabled(true);
        relayStopButton[i]->setAutoFillBackground(true);
        QPalette p = relayStopButton[i]->palette();
        p.setColor(QPalette::Button,this->cnf.getNotConnectedColor());
        relayStopButton[i]->setPalette(p);
        resultWidget.push_back(new ResultWidget(this));
        fullPassTimerLabel.push_back(new QLabel(this));
        QHBoxLayout *timeLayout = new QHBoxLayout;
        timeLayout->addWidget(new QLabel("Remaining time: ",this));
        timeLayout->addWidget(fullPassTimerLabel[i]);
        testResultLayout->addWidget(new QLabel("channel № " + QString::number(i),this),0,i,Qt::AlignCenter);
        testResultLayout->addLayout(timeLayout,1,i);
        testResultLayout->addWidget(relayStatusLabel[i],2,i);
        testResultLayout->addWidget(resultWidget[i],3,i);
        testResultLayout->addWidget(relayStopButton[i],4,i);
        connect(channelProcessing[i],&ChannelProcessing::colorStatus,relayStatusLabel[i],&QCLabel::setBackgroundColor);
        connect(channelProcessing[i],&ChannelProcessing::rrcPERSignal,resultWidget[i],&ResultWidget::setRrcPER);
        connect(channelProcessing[i],&ChannelProcessing::dlinkPERSignal,resultWidget[i],&ResultWidget::setDlinkPER);
        connect(channelProcessing[i],&ChannelProcessing::totalDlinkPERSignal,resultWidget[i],&ResultWidget::setTotalDlinkPER);
        connect(channelProcessing[i],&ChannelProcessing::lostDlinkSignal,resultWidget[i],&ResultWidget::setLost);
        connect(channelProcessing[i],&ChannelProcessing::setShutDownTime,resultWidget[i],&ResultWidget::setShutdownLabel);
        connect(channelProcessing[i],&ChannelProcessing::toLog,this,&TestWindow::writeToLog);
        connect(channelProcessingThread[i], &QThread::started,channelProcessing[i], &ChannelProcessing::start);
        connect(channelProcessingThread[i], &QThread::finished,channelProcessing[i], &ChannelProcessing::deleteLater);
        connect(testParamWidget,&TestParamWidget::zipFileState,channelProcessing[i],&ChannelProcessing::setComress);
        connect(relayStopButton[i],SIGNAL(clicked()),stopButtonMapper,SLOT(map()));
        stopButtonMapper->setMapping(relayStopButton[i], i);
        for(int k = 0; k < 2;k++){
            connect(radioRelay[k][i],&RadioRelay::haveModel,channelProcessing[i],&ChannelProcessing::setModel);
            connect(radioRelay[k][i],&RadioRelay::haveSerialNumber,channelProcessing[i],&ChannelProcessing::setSerialNumber);
        }

    }
    test = new Test(dlink3120,radioRelay,channelProcessing,cnf,this);
    QGroupBox *testResultGroupBox = new QGroupBox("Results ",this);
    testResultGroupBox->setLayout(testResultLayout);
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(testParamWidget);
    mainLayout->addWidget(testResultGroupBox);
    mainLayout->addWidget(startTestBut);

    setLayout(mainLayout);

    connect(stopButtonMapper, SIGNAL(mapped(int)),test, SLOT(stopChannel(int)));//кнопка stop
    connect(startTestBut,&QPushButton::clicked,test,&Test::startTest);
    connect(test,&Test::updateOnePassTimerLabel,testParamWidget,&TestParamWidget::setOnePassTimer);
    connect(test,&Test::updateFullPassTimerLabel,this,&TestWindow::setFullPassTimer);
    //connect(test,SIGNAL(updateFullPassTimerLabel(QVector<QString>&)),
    connect(testParamWidget,&TestParamWidget::timeOnePassChanged,test,&Test::setOnePassInterval);
    connect(testParamWidget,&TestParamWidget::timeFullPassChanged,test,&Test::setFullPassInterval);
    connect(test,&Test::testActive,testParamWidget,&TestParamWidget::setDisabled);
    connect(test,&Test::testActive,startTestBut,&QPushButton::setDisabled);
    connect(test,&Test::channelActive,this,&TestWindow::setEnabledStopButton);
    connect(test,&Test::testActive,this,&TestWindow::testActive);
    connect(test,&Test::channelActive,this,&TestWindow::channelActive);
    connect(test,&Test::toLog,this,&TestWindow::writeToLog);

    for(int i = 0; i < channelProcessing.size();i++)
        channelProcessingThread[i]->start();
    for (int i = 0; i < 3; i++)
      connect(test, &Test::escalateAttValue, attenuatorPPC[i], &AttenuatorPPC::escalateValue);
      testParamWidget->setDefaultValue("30","7");

    logFile = new QFile("file_log.log");
    logFile->open(QFile::WriteOnly);

}

void TestWindow::writeToLog(QString str){
    if(logFile->isOpen()){
        QTextStream logStream(logFile);
        logStream << str + "\n";
    }
}

void TestWindow::setEnabledStopButton(const int &numButton, const bool &state)///
{
    if(numButton >= relayStopButton.size())
        return;

    relayStopButton[numButton]->setEnabled(state);
    QPalette p = relayStopButton[numButton]->palette();

    if(state)
        p.setColor(QPalette::Button,this->cnf.getConnectedColor());
    else
        p.setColor(QPalette::Button,this->cnf.getNotConnectedColor());

    relayStopButton[numButton]->setPalette(p);
}

void TestWindow::setFullPassTimer(const QStringList str)
{
    for(int i = 0;i < str.size(); i++)
        fullPassTimerLabel[i]->setText(str[i]);
}

TestWindow::~TestWindow()
{
     logFile->close();

    for(int i = 0; i < channelProcessingThread.size(); i++){
        channelProcessingThread[i]->quit();
        channelProcessingThread[i]->wait();
    }
}


