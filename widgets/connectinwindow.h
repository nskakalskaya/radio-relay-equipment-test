#ifndef CONNECTINWINDOW_H
#define CONNECTINWINDOW_H

#include <QWidget>
#include <QStackedWidget>
#include <QStringList>
#include "connectwidget.h"
#include "attconnectwidget.h"
#include "dlinkwidget.h"
#include "dlink/dlink3120new.h"
#include "RRC/radiorelaynew.h"
#include "attenuator/attenuatorppc.h"
#include "config.h"


class ConnectinWindow : public QWidget
{
    Q_OBJECT
    Config &cnf;
    QComboBox *changeChannel; //виджет выбора канала
    const unsigned int connectWidgetSize;
    QVector <ConnectWidget *> connectWidget[2];
    QVector <RadioRelay *> radioRelay[2];
//    QVector <AttConnectWidget *> attConnectWidget;
    QVector <AttenuatorPPC *> attenuatorPPC;
    QVector <DlinkWidget *> dlinkWidget;
    QVector <Dlink3120new * > dlink3120;
//    QVector <QCheckBox *> turnHeater,turnAtt;
    QStackedWidget *connectChannelStack;

public:
    explicit ConnectinWindow(QStringList &channelList,
                             QVector<Dlink3120new * > &dlink3120,
                             QVector<RadioRelay *> (&radioRelay)[2],
                             QVector <AttenuatorPPC *> &attenuatorPPC,
                             Config &cnf,
                             QWidget *parent = 0);

    void setChannelList(QStringList &channelList);
    void blockChannelConnect(const int &numChannel,const bool &state);
    void blockDlinkConnect(const bool &state);

};

#endif // CONNECTINWINDOW_H
