#ifndef PARAMETERSWINDOW_H
#define PARAMETERSWINDOW_H

#include <QWidget>
#include <QStackedWidget>
#include <QPushButton>
#include <QGroupBox>
#include <QLineEdit>
#include <QLabel>
#include <QLayout>
#include <QTimer>
#include <QThread>
#include <QtAlgorithms>
#include <QMetaobject>
#include "test.h"
#include "attenuator/attenuatorppc.h"
#include "connectwidget.h"
#include "attconnectwidget.h"
#include "config.h"
#include "RRC/radiorelaynew.h"
#include "paramsWindowWidgets/heatparamWidget.h"
#include "paramsWindowWidgets/socketsparamwidget.h"
#include "testWindowWidgets/resultwidget.h"
#include "testWindowWidgets/testparamwidget.h"

class ParametersWindow : public QWidget
{
    Q_OBJECT
    Config &cnf;
    QComboBox *changeChannel; //виджет выбора канала
    QVector <RadioRelay *> radioRelay[2];
    QVector <AttConnectWidget *> attConnectWidget;
    QVector <AttenuatorPPC *> attenuatorPPC;
    QStringList channelList;
    QStackedWidget *connectChannelStack;
    HeatParamWidget *heatParamWidget;
    SocketsParamWidget *socketsParamWidget;

public:
    explicit ParametersWindow(QStringList &channelList,
                        QVector<RadioRelay *> (&radioRelay)[2],
                        QVector <AttenuatorPPC *> &attenuatorPPC,
                        Config &cnf,
                        QWidget *parent = 0);
signals:

public slots:
    void disableHeat(const bool& state);
 //   void disableAtt(const bool& state);
    void disableSockets(const bool& state);
 //   void blockAttWidgets(const int &numChannel, const bool &state);
};

#endif // PARAMETERSWINDOW_H
