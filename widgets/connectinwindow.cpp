#include "connectinwindow.h"

ConnectinWindow::ConnectinWindow(QStringList &channelList, QVector<Dlink3120new * > &dlink3120, QVector<RadioRelay *> (&radioRelay)[2], QVector<AttenuatorPPC *> &attenuatorPPC,Config &cnf, QWidget *parent) :
    QWidget(parent),
    dlink3120(dlink3120),
    radioRelay(radioRelay),
    attenuatorPPC(attenuatorPPC),
    connectWidgetSize(2),
    cnf(cnf)
{
    changeChannel = new QComboBox(this);
    connectChannelStack = new QStackedWidget(this);
    setChannelList(channelList);

    QVBoxLayout *stackedLayout[channelList.size()];
    QHBoxLayout *relayLayout[channelList.size()];
    QVBoxLayout *paramLayout[channelList.size()];
    QHBoxLayout *attAndParamLayout[channelList.size()];
    QWidget *stackedWidget[channelList.size()];

    /*rrc & att*/
    for(int i = 0; i < channelList.size(); i++) {
        stackedLayout[i] = new QVBoxLayout;
        relayLayout[i] = new QHBoxLayout;
        for(int k = 0; k < connectWidgetSize; k++){
            connectWidget[k].push_back(new ConnectWidget(i,k,cnf,this));
            relayLayout[i]->addWidget(connectWidget[k][i]);
            connect(connectWidget[k][i],&ConnectWidget::connectToRadioRelay,radioRelay[k][i],&RadioRelay::connectToSnmp);
            connect(connectWidget[k][i],&ConnectWidget::disconnectFromRadioRelay,radioRelay[k][i],&RadioRelay::disconnectFromSnmp);
            connect(radioRelay[k][i],&RadioRelay::haveID,connectWidget[k][i],&ConnectWidget::fillTextEditRadioRelay);
            connect(radioRelay[k][i],&RadioRelay::disconnected,connectWidget[k][i],&ConnectWidget::disconnected);
            connect(radioRelay[k][i],&RadioRelay::connected,connectWidget[k][i],&ConnectWidget::connected);
        }
        stackedLayout[i]->addLayout(relayLayout[i]);
        stackedWidget[i] = new QWidget(this);
        stackedWidget[i]->setLayout(stackedLayout[i]);
        connectChannelStack->addWidget(stackedWidget[i]);
    }

    /*dlink*/
    QHBoxLayout *dlinkLayout = new QHBoxLayout;
    for(int i = 0; i < dlink3120.size(); i++){
        dlinkWidget.push_back(new DlinkWidget(i,cnf,this));
        dlinkLayout->addWidget(dlinkWidget[i]);
        connect(dlinkWidget[i],&DlinkWidget::connectToDlink,dlink3120[i],&Dlink3120new::connectToSnmp);
        connect(dlinkWidget[i],&DlinkWidget::disconnectFromDlink,dlink3120[i],&Dlink3120new::disconnectFromSnmp);
        connect(dlink3120[i],&Dlink3120new::disconnected,dlinkWidget[i],&DlinkWidget::disconnected);
        connect(dlink3120[i],&Dlink3120new::connected,dlinkWidget[i],&DlinkWidget::connected);
    }
    dlinkWidget[0]->setText(cnf.getDlinkIp0());
    dlinkWidget[1]->setText(cnf.getDlinkIp1());
    connect(dlinkWidget[0],&DlinkWidget::textChanged,&this->cnf,&Config::setDlinkIp0);
    connect(dlinkWidget[1],&DlinkWidget::textChanged,&this->cnf,&Config::setDlinkIp1);

    /*main layout*/
    QVBoxLayout *mLayout = new QVBoxLayout;
    mLayout->addWidget(changeChannel);
    mLayout->addWidget(connectChannelStack);
    mLayout->addLayout(dlinkLayout);
    mLayout->addStretch(1);

    setLayout(mLayout);

    /*connect*/
    //connect(changeChannel,&QComboBox::currentIndexChanged,connectChannelStack,&QStackedWidget::setCurrentIndex);
    connect(changeChannel,SIGNAL(currentIndexChanged(int)),connectChannelStack,SLOT(setCurrentIndex(int)));
}

void ConnectinWindow::setChannelList(QStringList &channelList)
{
    //channelList - список задейственных портов
    changeChannel->clear();
    for(int i = 0; i < channelList.size(); i++)
        changeChannel->addItem("Channel № " + QString::number(i) + " ( port № " + channelList[i] + ")");
}

void ConnectinWindow::blockChannelConnect(const int &numChannel,const bool &state)///
{
//    attConnectWidget[numChannel]->setDisabled(state);
    connectWidget[0][numChannel]->setDisabled(state);
    connectWidget[1][numChannel]->setDisabled(state);
//    turnHeater[numChannel]->setDisabled(state);
 //   turnAtt[numChannel]->setDisabled(state);
}

void ConnectinWindow::blockDlinkConnect(const bool &state)///
{
    dlinkWidget[0]->setDisabled(state);
    dlinkWidget[1]->setDisabled(state);
}
