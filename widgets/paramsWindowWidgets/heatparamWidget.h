#ifndef HEATPARAMWIDGET_H
#define HEATPARAMWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QGroupBox>
#include <QLayout>
#include <QLineEdit>
#include <QCheckBox>

class HeatParamWidget : public QWidget
{
    Q_OBJECT
    QLineEdit *maxTempLineEdit,*heatTimeLineEdit, *coolTimeLineEdit;
    QCheckBox *turnHeater;
    void convertToIntHeat(const QString& str);
    void convertToIntCool(const QString& str);
    void convertToFloatMaxTemp(const QString &str);
public:
    explicit HeatParamWidget(QWidget *parent = 0);
    void setDefaultValue(const QString& maxTemp, const QString& heatTimeMin, const QString& coolTimeMin);
    ~HeatParamWidget();

signals:
    void maxTempChanged(const float& maxTemp);
    void heatTimeChanged(const int& mSec);
    void coolTimeChanged(const int& mSec);
    void setHeaterSignal(bool ifChecked);

};

#endif // HEATPARAMWIDGET_H
