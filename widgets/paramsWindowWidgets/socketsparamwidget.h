#ifndef SOCKETSPARAMWIDGET_H
#define SOCKETSPARAMWIDGET_H

#include <QWidget>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QCheckBox>

class SocketsParamWidget : public QWidget
{
    Q_OBJECT
    QCheckBox *turnSockets;

public:
    explicit SocketsParamWidget(QWidget *parent = 0);

signals:

public slots:
};

#endif // SOCKETSPARAMWIDGET_H
