#include "socketsparamwidget.h"

SocketsParamWidget::SocketsParamWidget(QWidget *parent) : QWidget(parent),
    turnSockets(new QCheckBox("Turn on sockets", this))
{
    turnSockets->setChecked(false);
    QGroupBox *socketsParamGroupBox = new QGroupBox("Sockets parameters", this);
    QHBoxLayout *socketsLayout = new QHBoxLayout;
    QHBoxLayout *mainLayout = new QHBoxLayout;
    socketsLayout->addWidget(turnSockets);
    socketsParamGroupBox->setLayout(socketsLayout);
    mainLayout->addWidget(socketsParamGroupBox);
    setLayout(mainLayout);
}
