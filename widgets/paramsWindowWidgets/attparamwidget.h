#ifndef ATTPARAMWIDGET_H
#define ATTPARAMWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QGroupBox>
#include <QLayout>
#include <QDebug>

class AttParamWidget : public QWidget
{
    Q_OBJECT
    QLineEdit *minDbLineEdit,*maxDbLineEdit;
public:
    explicit AttParamWidget(QWidget *parent = 0);
    ~AttParamWidget();

signals:
    void maxDbChanged(const QString& str);
    void minDbChanged(const QString& str);
    void sendMinDbValue(int min);
    void sendMaxDbValue(int max);

public slots:
    void setMaxDbChanged(const QString &str);
    void setMinDbChanged(const QString &str);
    void sendValues();
};

#endif // ATTPARAMWIDGET_H
