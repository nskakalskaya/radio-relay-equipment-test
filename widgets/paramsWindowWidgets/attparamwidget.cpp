#include "attparamwidget.h"


AttParamWidget::AttParamWidget(QWidget *parent) : QWidget(parent),
    minDbLineEdit(new QLineEdit(this)),
    maxDbLineEdit(new QLineEdit(this))
{
    minDbLineEdit->setFixedWidth(50);
    maxDbLineEdit->setFixedWidth(50);

    QGroupBox *attParamGroupBox = new QGroupBox("Attenuator parameters",this);
    QGridLayout *dbLayout = new QGridLayout;
    QHBoxLayout *attParamLayout = new QHBoxLayout;
    dbLayout->addWidget(new QLabel("min (dB)",this),0,0);
    dbLayout->addWidget(minDbLineEdit,0,1);
    dbLayout->addWidget(new QLabel("max (dB)",this),1,0);
    dbLayout->addWidget(maxDbLineEdit,1,1);
    minDbLineEdit->setText("40");
    maxDbLineEdit->setText("70");
    attParamLayout->addLayout(dbLayout);
    attParamLayout->addStretch(1);
    attParamGroupBox->setLayout(attParamLayout);
    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(attParamGroupBox);
    connect(minDbLineEdit,&QLineEdit::textChanged,this,&AttParamWidget::minDbChanged);
    connect(maxDbLineEdit,&QLineEdit::textChanged,this,&AttParamWidget::maxDbChanged);
    setLayout(mainLayout);
}

void AttParamWidget::setMaxDbChanged(const QString &str)
{
    maxDbLineEdit->setText(str);
}

void AttParamWidget::setMinDbChanged(const QString &str)
{
    minDbLineEdit->setText(str);
}

void AttParamWidget::sendValues()
{
    emit sendMinDbValue((minDbLineEdit->text()).toInt());
    emit sendMaxDbValue((maxDbLineEdit->text()).toInt());
}

AttParamWidget::~AttParamWidget()
{

}

