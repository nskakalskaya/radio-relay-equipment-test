#include "heatparamwidget.h"

HeatParamWidget::HeatParamWidget(QWidget *parent) : QWidget(parent),
    maxTempLineEdit(new QLineEdit(this)),
    heatTimeLineEdit(new QLineEdit(this)),
    coolTimeLineEdit(new QLineEdit(this))
{

    maxTempLineEdit->setFixedWidth(50);
    heatTimeLineEdit->setFixedWidth(50);
    coolTimeLineEdit->setFixedWidth(50);
    turnHeater = new QCheckBox("Turn on a heater",this);
    turnHeater->setChecked(false);
    QGroupBox *heatParamGroupBox = new QGroupBox("Heat parameters",this);
    QGridLayout *heatLayout = new QGridLayout;
    QHBoxLayout *heatParamLayout = new QHBoxLayout;
    heatLayout->addWidget(turnHeater, 0, 0);
    heatLayout->addWidget(new QLabel("max\ntemperature",this),1,0);
    heatLayout->addWidget(maxTempLineEdit,1,1);
    heatLayout->addWidget(new QLabel("time of  maintaining\nmax temperature",this),2,0);
    heatLayout->addWidget(heatTimeLineEdit,2,1);
    heatLayout->addWidget(new QLabel("cooling\ntime",this),3,0);
    heatLayout->addWidget(coolTimeLineEdit,3,1);
    heatParamLayout->addLayout(heatLayout);
    heatParamLayout->addStretch(1);
    heatParamGroupBox->setLayout(heatParamLayout);
    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(heatParamGroupBox);
    connect(maxTempLineEdit,&QLineEdit::textChanged,this,&HeatParamWidget::convertToFloatMaxTemp);
    connect(heatTimeLineEdit,&QLineEdit::textChanged,this,&HeatParamWidget::convertToIntHeat);
    connect(coolTimeLineEdit,&QLineEdit::textChanged,this,&HeatParamWidget::convertToIntCool);
    connect(turnHeater, &QCheckBox::toggled, [=] {
        maxTempLineEdit->setEnabled(turnHeater->isChecked());
        heatTimeLineEdit->setEnabled(turnHeater->isChecked());
        coolTimeLineEdit->setEnabled(turnHeater->isChecked());
        emit setHeaterSignal(turnHeater->isChecked());
    });
    setLayout(mainLayout);
}

void HeatParamWidget::setDefaultValue(const QString& maxTemp, const QString& heatTimeMin, const QString& coolTimeMin)
{
    maxTempLineEdit->setText(maxTemp);
    heatTimeLineEdit->setText(heatTimeMin);
    coolTimeLineEdit->setText(coolTimeMin);
}

void HeatParamWidget::convertToIntHeat(const QString& str)
{
    emit heatTimeChanged(str.toInt() * 60 * 1000);
}

void HeatParamWidget::convertToIntCool(const QString& str)
{
    emit coolTimeChanged(str.toInt() * 60 * 1000);
}

void HeatParamWidget::convertToFloatMaxTemp(const QString &str)
{
    emit maxTempChanged(str.toFloat());
}

HeatParamWidget::~HeatParamWidget()
{

}

