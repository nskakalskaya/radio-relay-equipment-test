#ifndef RESULTWIDGET_H
#define RESULTWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QGroupBox>
#include <QLayout>

class ResultWidget : public QWidget
{
    Q_OBJECT
    QLabel *rrcLabel[2];
    QLabel *dlinkLabel[2];
    QLabel *totalDlinkLabel[2];
    QLabel *lostLabel[2];
    QLabel *shutDownLabel;
public:
    explicit ResultWidget(QWidget *parent = 0);
    ~ResultWidget();

public slots:
    void setRrcPER(const int &numRadio,const double &PER);///
    void setDlinkPER(const int &numDlink, const double &PER);///
    void setTotalDlinkPER(const int& numDlink,const double& PER);
    void setLost(const int& numDlink,const double& lost);
    void setShutdownLabel(const QString& str);
};

#endif // RESULTWIDGET_H
