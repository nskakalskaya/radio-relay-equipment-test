#include "testparamwidget.h"

TestParamWidget::TestParamWidget(QWidget *parent) : QWidget(parent),
    timeOnePassLineEdit(new QLineEdit(this)),
    timeFullPassLineEdit(new QLineEdit(this)),
    onePassTimerLabel(new QLabel(this)),
    fullPassTimerLabel(new QLabel(this))
{

    QCheckBox *zipCheckBox = new QCheckBox("Archive", this);
    zipCheckBox->setChecked(false);
    QGroupBox *testParamGroupBox = new QGroupBox("Test parameters",this);
    QGridLayout *passLayout = new QGridLayout;
    QHBoxLayout *testParamLayout = new QHBoxLayout;
    passLayout->addWidget(new QLabel("Time of one pass (minutes)",this),0,0);
    passLayout->addWidget(timeOnePassLineEdit,0,1);
    passLayout->addWidget(new QLabel("Estimated time",this),1,0);
    passLayout->addWidget(onePassTimerLabel,1,1);
    passLayout->addWidget(new QLabel("Time of full test (days)",this),2,0);
    passLayout->addWidget(timeFullPassLineEdit,2,1);
    passLayout->addWidget(zipCheckBox,3,0,3,2);
    testParamLayout->addLayout(passLayout);
    testParamLayout->addStretch(1);
    testParamGroupBox->setLayout(testParamLayout);

    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->addWidget(testParamGroupBox);
    setLayout(mainLayout);
    connect(timeOnePassLineEdit,&QLineEdit::textChanged,this,&TestParamWidget::convertToIntOnePass);
    connect(timeFullPassLineEdit,&QLineEdit::textChanged,this,&TestParamWidget::convertToIntFullPass);
    connect(zipCheckBox,&QCheckBox::clicked,this,&TestParamWidget::zipFileState);

    timeFullPassLineEdit->setFixedWidth(50);
    timeOnePassLineEdit->setFixedWidth(50);
    timeFullPassLineEdit->setInputMask("99");
    timeOnePassLineEdit->setInputMask("99");
}

void TestParamWidget::setDefaultValue(const QString &onePass, const QString &fullPass)
{
    timeFullPassLineEdit->setText(fullPass);
    timeOnePassLineEdit->setText(onePass);
}

void TestParamWidget::convertToIntOnePass(const QString &str)
{
    emit timeOnePassChanged(str.toInt() * 60 * 1000);//на одну минуту
}

void TestParamWidget::convertToIntFullPass(const QString &str)
{
    quint64 a = str.toInt() * 24 * 60 * 60 * 1000;
    emit timeFullPassChanged(str.toInt() * 24 * 60 * 60 * 1000);//на один день
}

void TestParamWidget::setOnePassTimer(const QString &str)
{
    onePassTimerLabel->setText(str);
}

/*void TestParamWidget::setFullPassTimer(const QString &str)
{
    fullPassTimerLabel->setText(str);
}*/

TestParamWidget::~TestParamWidget()
{

}

