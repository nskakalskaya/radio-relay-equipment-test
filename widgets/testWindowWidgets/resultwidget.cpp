#include "resultwidget.h"

ResultWidget::ResultWidget(QWidget *parent) : QWidget(parent)
{
    QGridLayout *mainLayout = new QGridLayout;
    for(int i = 0; i < 2; i++){
        rrcLabel[i] = new QLabel(this);
        mainLayout->addWidget(new QLabel("RRC " + QString::number(i),this),0 + i,0);
        mainLayout->addWidget(rrcLabel[i],0 + i,1);
    }
    for(int i = 0; i < 2; i++){
        dlinkLabel[i] = new QLabel(this);
        mainLayout->addWidget(new QLabel("dlink " + QString::number(i),this),2 + i,0);
        mainLayout->addWidget(dlinkLabel[i],2 + i,1);
    }
    for(int i = 0; i < 2; i++){
        totalDlinkLabel[i] = new QLabel(this);
        mainLayout->addWidget(new QLabel("total dlink " + QString::number(i),this),4 + i,0);
        mainLayout->addWidget(totalDlinkLabel[i],4 + i,1);
    }
    for(int i = 0; i < 2; i++){
        lostLabel[i] = new QLabel(this);
        mainLayout->addWidget(new QLabel("lost " + QString::number(i),this),6 + i,0);
        mainLayout->addWidget(lostLabel[i],6 + i,1);
    }
        shutDownLabel = new QLabel(this);
        mainLayout->addWidget(new QLabel("shutdown\nwithin ",this),8,0);
        mainLayout->addWidget(shutDownLabel,8,1);
    setLayout(mainLayout);
}

void ResultWidget::setRrcPER(const int &numRadio, const double &PER)
{
    rrcLabel[numRadio]->setNum(PER);
}

void ResultWidget::setDlinkPER(const int &numDlink,const double &PER)
{
    dlinkLabel[numDlink]->setNum(PER);
}

void ResultWidget::setTotalDlinkPER(const int &numDlink, const double &PER)
{
    totalDlinkLabel[numDlink]->setNum(PER);
}

void ResultWidget::setLost(const int& numDlink, const double &lost)
{
    lostLabel[numDlink]->setNum(lost);
}

void ResultWidget::setShutdownLabel(const QString &str)
{
    shutDownLabel->setText(str);
}

ResultWidget::~ResultWidget()
{

}

