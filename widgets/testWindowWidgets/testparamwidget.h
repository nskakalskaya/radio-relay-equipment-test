#ifndef TESTPARAMWIDGET_H
#define TESTPARAMWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QGroupBox>
#include <QLayout>
#include <QLineEdit>
#include <QCheckBox>

class TestParamWidget : public QWidget
{
    Q_OBJECT
    QLineEdit *timeOnePassLineEdit,*timeFullPassLineEdit;
    QLabel *onePassTimerLabel,*fullPassTimerLabel;
    void convertToIntOnePass(const QString& str);
    void convertToIntFullPass(const QString& str);
public:
    explicit TestParamWidget(QWidget *parent = 0);
    void setDefaultValue(const QString & onePass, const QString & fullPass);
    ~TestParamWidget();

signals:
    void timeOnePassChanged(const int& time);
    void timeFullPassChanged(const quint64& time);
    void zipFileState(bool state);
public slots:
    void setOnePassTimer(const QString& str);//время прошедшее со времени старта прохода
    //void setFullPassTimer(const QString& str);//оставшееся время до окончания теста
};

#endif // TESTPARAMWIDGET_H
