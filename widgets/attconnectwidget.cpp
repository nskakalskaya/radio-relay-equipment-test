#include "attconnectwidget.h"

AttConnectWidget::AttConnectWidget(Config &cnf, int attNum, QWidget *parent) : QWidget(parent),
    connectToAttButton(new QPushButton("Connect",this)),
    cnf(cnf),
    attNumber(attNum),
    ifConnected(false)
{
     attParamWidget = new AttParamWidget(this);
     connectToAttButton->setEnabled(false);
     QGridLayout *inputLayout = new QGridLayout;
     turnAttBox = new QCheckBox("Turn on an attenuator");
       connect(turnAttBox, &QCheckBox::toggled, [=]{
           turnOnAtt(turnAttBox->isChecked(), attNumber-1);
           connectToAttButton->setEnabled(turnAttBox->isChecked());
       });
       inputLayout->addWidget(connectToAttButton, 1, 0);
       inputLayout->addWidget(turnAttBox, 0, 0);
       inputLayout->addWidget(attParamWidget, 2, 0);
       QString name = "Attenuator box " + QString::number(attNumber);
       QGroupBox *attBox = new QGroupBox(name, this);
       attBox->setLayout(inputLayout);
       QVBoxLayout *mainLayout = new QVBoxLayout;
       mainLayout->addWidget(attBox);
       QPalette p = connectToAttButton->palette();
        p.setColor(QPalette::Button,this->cnf.getNotConnectedColor());
        connectToAttButton->setPalette(p);
        connectToAttButton->setAutoFillBackground(true);
        setLayout(mainLayout);
        connect(connectToAttButton,&QPushButton::clicked,[=] {
            pressedConToAtt(attNumber - 1);
        });
        connect(this, &AttConnectWidget::sendMinMaxValues, attParamWidget, &AttParamWidget::sendValues);
        connect(attParamWidget, &AttParamWidget::sendMaxDbValue, this, &AttConnectWidget::sendMaxDbValue);
        connect(attParamWidget, &AttParamWidget::sendMinDbValue, this, &AttConnectWidget::sendMinDbValue);
}

void AttConnectWidget::pressedConToAtt(int deviceIndex)
{
    if (ifConnected) {
        emit disconnectFromAttenuator();
        ifConnected = false;
       // emit stopEscalating();
    }
    else {
        emit connectToAttenuator(deviceIndex);
        emit startEscalating();
        ifConnected = true;
    }
}

void AttConnectWidget::fillTextEditAtt(const QString &str)
{
//    attTextEdit->clear();
//    attTextEdit->append(str);
}

void AttConnectWidget::turnOnAtt(bool state, int deviceIndex)
{
    if (state)
        emit sendMinMaxValues();
//    attPort->setEnabled(state);
//    attTextEdit->setEnabled(state);
    //    connectToAttButton->setEnabled(state);
}

void AttConnectWidget::connected()
{
    connectToAttButton->setText("Disconnect");
    disconnect(connectToAttButton,&QPushButton::clicked,this,&AttConnectWidget::pressedConToAtt);
    connect(connectToAttButton,&QPushButton::clicked,this,&AttConnectWidget::disconnectFromAttenuator);
    QPalette p = connectToAttButton->palette();
    p.setColor(QPalette::Button,cnf.getConnectedColor());
    connectToAttButton->setPalette(p);
}

void AttConnectWidget::disconnected()
{
    connectToAttButton->setText("Connect");
    disconnect(connectToAttButton,&QPushButton::clicked,this, &AttConnectWidget::disconnectFromAttenuator);
    connect(connectToAttButton,&QPushButton::clicked,this, &AttConnectWidget::pressedConToAtt);
    QPalette p = connectToAttButton->palette();
    p.setColor(QPalette::Button,cnf.getNotConnectedColor());
    connectToAttButton->setPalette(p);
    //attTextEdit->clear();
}

AttConnectWidget::~AttConnectWidget()
{

}

