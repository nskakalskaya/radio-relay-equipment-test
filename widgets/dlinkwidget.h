#ifndef DLINKWIDGET_H
#define DLINKWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QGroupBox>
#include <QLabel>
#include <QLayout>
#include <QKeyEvent>
#include "config.h"

class DlinkWidget : public QWidget
{
    Q_OBJECT
    QLineEdit *ipDlink;    
    QPushButton *connectDlinkBut;
    Config &cnf;

public:
    explicit DlinkWidget(const unsigned int &numDlink, Config &cnf, QWidget *parent = 0);
    void setText(const QString& str);///
    ~DlinkWidget();

protected:
    virtual void keyPressEvent(QKeyEvent *event);

signals:
    void connectToDlink(const QString &address);
    void textChanged(const QString &str);
    void disconnectFromDlink();

public slots:
    void pressedConToDlink();
    void connected();
    void disconnected();
};

#endif // DLINKWIDGET_H
