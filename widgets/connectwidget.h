#ifndef CONNECTWIDGET_H
#define CONNECTWIDGET_H

#include <QWidget>
#include <Qlabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QPushButton>
#include <QLayout>
#include <QGroupBox>
#include <QKeyEvent>
#include "config.h"


class ConnectWidget : public QWidget
{
    Q_OBJECT
    QLineEdit *ip;
    QTextEdit *radTextEdit;
    QPushButton *connectToRadioButton;
    const unsigned int numChannel,numRadio;
    Config &cnf;
public:
    explicit ConnectWidget(const unsigned int& numChannel,const unsigned int& numRadio, Config &cnf, QWidget *parent = 0);
    ~ConnectWidget();

protected:
    virtual void keyPressEvent(QKeyEvent * event);

signals:
    void connectToRadioRelay(const QString &ip);
    void disconnectFromRadioRelay();

public slots:
    void pressedConnectToRadioRelay();
    void fillTextEditRadioRelay(const QString &str);
    void connected();
    void disconnected();
};

#endif // CONNECTWIDGET_H
