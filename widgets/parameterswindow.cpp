#include "parameterswindow.h"

ParametersWindow::ParametersWindow(QStringList &channelList, QVector<RadioRelay *> (&radioRelay)[2], QVector<AttenuatorPPC *> &attenuatorPPC, Config &cnf, QWidget *parent) : QWidget(parent),
    cnf(cnf),
    radioRelay(radioRelay),
    attenuatorPPC(attenuatorPPC),
    channelList(channelList)
{
    heatParamWidget = new HeatParamWidget(this);
    socketsParamWidget = new SocketsParamWidget(this);
    QHBoxLayout *attHeatParamLayout = new QHBoxLayout;
    attHeatParamLayout->addWidget(heatParamWidget);
    attHeatParamLayout->addWidget(socketsParamWidget);
    connectChannelStack = new QStackedWidget(this);
    QVBoxLayout *stackedLayout, *paramLayout;
    QGridLayout *attAndParamLayout;
    QWidget *stackedWidget = new QWidget(this);
    QHBoxLayout *attLayout = new QHBoxLayout;
    stackedLayout = new QVBoxLayout;
    attAndParamLayout = new QGridLayout;
    paramLayout = new QVBoxLayout;
    for (int i = 0; i < channelList.size(); i++) {
        connect(heatParamWidget, &HeatParamWidget::setHeaterSignal, radioRelay[0][i],&RadioRelay::setHeater);
        connect(heatParamWidget, &HeatParamWidget::setHeaterSignal, radioRelay[1][i],&RadioRelay::setHeater);
    }
    for (auto i = 0; i<3; i++) {
        attConnectWidget.push_back(new AttConnectWidget(cnf, i+1, this));
        attLayout->addWidget(attConnectWidget[i]);
        connect(attConnectWidget[i], &AttConnectWidget::connectToAttenuator, attenuatorPPC[i], &AttenuatorPPC::connectToAttenuator);
        connect(attConnectWidget[i], &AttConnectWidget::disconnectFromAttenuator, attenuatorPPC[i], &AttenuatorPPC::disconnectFromAttenuator);
        connect(attConnectWidget[i], &AttConnectWidget::startEscalating, attenuatorPPC[i], &AttenuatorPPC::startEscalating);
        connect(attenuatorPPC[i], &AttenuatorPPC::connected, attConnectWidget[i], &AttConnectWidget::connected);
        connect(attenuatorPPC[i], &AttenuatorPPC::disconnected, attConnectWidget[i], &AttConnectWidget::disconnected);
        connect(attenuatorPPC[i],&AttenuatorPPC::haveID,attConnectWidget[i],&AttConnectWidget::fillTextEditAtt);
        connect(attConnectWidget[i], &AttConnectWidget::sendMaxDbValue, attenuatorPPC[i], &AttenuatorPPC::getMaxDb);
        connect(attConnectWidget[i], &AttConnectWidget::sendMinDbValue, attenuatorPPC[i], &AttenuatorPPC::getMinDb);
     }
     for (int k = 0; k < 2; k++)
        for (int i = 0; i < radioRelay[k].size(); i++) {
                connect(heatParamWidget, &HeatParamWidget::maxTempChanged,radioRelay[k][i],&RadioRelay::setMaxTemp);
                connect(heatParamWidget, &HeatParamWidget::heatTimeChanged,radioRelay[k][i],&RadioRelay::setHeatTime);
                connect(heatParamWidget, &HeatParamWidget::coolTimeChanged,radioRelay[k][i],&RadioRelay::setCoolTime);
        }
    attAndParamLayout->addLayout(attLayout, 0, 0);
    attAndParamLayout->addLayout(attHeatParamLayout, 1, 0);
    attAndParamLayout->addLayout(paramLayout, 2, 0);
    stackedWidget->setLayout(attAndParamLayout);
    connectChannelStack->addWidget(stackedWidget);
    heatParamWidget->setDefaultValue("75","30","90");
}

void ParametersWindow::disableHeat(const bool& state)
{
    emit heatParamWidget->HeatParamWidget::setDisabled(state);
}

//void ParametersWindow::disableAtt(const bool &state)
//{
//    emit attParamWidget->AttParamWidget::setDisabled(state);
//}

void ParametersWindow::disableSockets(const bool &state)
{
    emit socketsParamWidget->SocketsParamWidget::setDisabled(state);
}

//void ParametersWindow::blockAttWidgets(const int &numChannel, const bool &state)
//{
//   for (auto i = 0; i < 3; i++)
//       emit attConnectWidget[i]->AttConnectWidget::setDisabled(state);
//}
