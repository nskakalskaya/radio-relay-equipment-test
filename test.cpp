#include "test.h"
#include <QDebug>
///TODO

Test::Test(QVector<Dlink3120new *> &dlink3120, QVector<RadioRelay *> (&radioRelay)[2], QVector<ChannelProcessing *> &channelProcessing, Config &cnf, QObject *parent) : QObject(parent),
    dlink3120(dlink3120),
    radioRelay(radioRelay),
    channelProcessing(channelProcessing),
    onePassTimer(new QTimer(this)),
    //fullPassTimer(new QTimer(this)),
    trafficGeneratorPort(cnf.getGeneratorPort()),
    oneDay(24 * 60 * 60 * 1000),
    testIsActive(false)
{
    updateTimer = new QTimer(this);
    updateTimer->setInterval(1000);
    fullPassTimer.resize(radioRelay[0].size());

    for(int i = 0; i < fullPassTimer.size(); i++){
        fullPassTimer[i] = new QTimer(this);
        fullPassTimer[i]->setSingleShot(true);
    }

    QSignalMapper *shutdownTestMapper = new QSignalMapper(this);
    QSignalMapper *stopChannelMapper = new QSignalMapper(this);
    disconnectChannelMapper[0] = new QSignalMapper(this);
    disconnectChannelMapper[1] = new QSignalMapper(this);

    channelIsActive.resize(radioRelay[0].size());
    channelIsActive.fill(0,channelIsActive.size());
    connect(onePassTimer,&QTimer::timeout,this,&Test::onePassTimeOut);
    connect(updateTimer,&QTimer::timeout,this,&Test::updateTimerLabel);

    /*Dlink connect*/
    for(int i = 0; i < dlink3120.size();i++){
        connect(this,&Test::clearALLstat,dlink3120[i],&Dlink3120new::cleanAllCounters);
        connect(this,&Test::turnOffDlink,dlink3120[i],&Dlink3120new::turnOffPort);
        connect(this,&Test::turnOnDlink,dlink3120[i],&Dlink3120new::turnOnPort);
        connect(this,&Test::getStatDlink,dlink3120[i],&Dlink3120new::getStat);
        connect(dlink3120[i],&Dlink3120new::statFromDlink,this,&Test::dlinkSeparate);
    }

    for(int i = 0; i < channelProcessing.size(); i++){
        connect(radioRelay[0][i],SIGNAL(disconnected()),disconnectChannelMapper[0],SLOT(map()));
        connect(radioRelay[1][i],SIGNAL(disconnected()),disconnectChannelMapper[1],SLOT(map()));
        //disconnectChannelMapper[0]->setMapping(radioRelay[0][i], i);
        //disconnectChannelMapper[1]->setMapping(radioRelay[1][i], i);

        connect(radioRelay[0][i], &RadioRelay::sendSignal, channelProcessing[i], &ChannelProcessing::getModelName);
       connect(radioRelay[1][i], &RadioRelay::sendSignal, channelProcessing[i], &ChannelProcessing::getModelName);

        connect(channelProcessing[i],SIGNAL(stopChannel()),stopChannelMapper,SLOT(map()));
        stopChannelMapper->setMapping(channelProcessing[i], i);

        /*условие выключение теста, является то что отчет файл создан*/
        connect(channelProcessing[i],SIGNAL(resultComlpete()),shutdownTestMapper,SLOT(map()));
        shutdownTestMapper->setMapping(channelProcessing[i], i);

       //connect(fullPassTimer[i],SIGNAL(timeout()),this,SLOT(slot()));
    }
    connect(stopChannelMapper, SIGNAL(mapped(int)),this, SLOT(stopChannel(int)));//прошли сутки с того момента, как релейка стала желтой или красной
    connect(shutdownTestMapper,SIGNAL(mapped(int)),this, SLOT(disconnectFromRRC(int)));//выключение каналов, после записи теста
    connect(disconnectChannelMapper[0], SIGNAL(mapped(int)),this, SLOT(stopChannel(int)));//потеряна связь с релейкой
    connect(disconnectChannelMapper[1], SIGNAL(mapped(int)),this, SLOT(stopChannel(int)));//потеряна связь с релейкой
}

void Test::startTest()
{
    checkChannelIsActive();
    bool channel = false;
    for(int i = 0; i < channelIsActive.size(); i++)
        channel += channelIsActive[i];            

    if(!channel)//есть активный канал - запускаем тест
        return;

    //подключены ли длинки
    bool dlink = dlink3120[0]->isConnected() && dlink3120[1]->isConnected();

    //заданы ли минимально необходимые параметры для запуска теста
    bool fullPassTimerParam = false;
    for(int i = 0; i < fullPassTimer.size(); i++){
        fullPassTimerParam += (fullPassTimer[i]->interval() > 0);
    }
    bool inputParam = onePassTimer->interval() > 0 && fullPassTimerParam;

    if(!dlink && !inputParam)
        return;


    for(int i = 0; i < channelProcessing.size(); i++){
        QMetaObject::invokeMethod(channelProcessing[i],"initialize");
    }

    for(int i = 0; i < channelIsActive.size(); i++){
            QMetaObject::invokeMethod(radioRelay[0][i],"startHeat",Qt::AutoConnection);//включение нагревателей
            QMetaObject::invokeMethod(radioRelay[1][i],"startHeat",Qt::AutoConnection);//включение нагревателей
    }


    connect(dlink3120[1],&Dlink3120new::portIsOn,this,&Test::createConnect);
    //сброс статистики на длинке
    emit turnOffDlink(trafficGeneratorPort);
    emit clearALLstat();
    emit turnOnDlink(trafficGeneratorPort);

    //запуск таймеров

    for(int i = 0; i < fullPassTimer.size(); i++){
        if(channelIsActive[i])
            fullPassTimer[i]->start();
    }

    onePassTimer->start();
    updateTimer->start();
    timeElapsed.start();
    //сигнал выключающий кнопки
    testIsActive = true;
    emit toLog("start test(test.cpp)");
    emit testActive(testIsActive);

}

void Test::checkChannelIsActive()
{
    //проверка включенных релеек

        for(int i = 0; i < channelIsActive.size(); i++){
            emit ("channel active 0 (test.cpp) i " + QString::number(i) + QString::number(radioRelay[0][i]->isConnected()));
            emit ("channel active 1 (test.cpp) i " + QString::number(i) + QString::number(radioRelay[1][i]->isConnected()));
            emit ("channelIsActive[] (test.cpp) i "  + QString::number(i) + QString::number(channelIsActive[i]));

            if(radioRelay[0][i]->isConnected() && radioRelay[1][i]->isConnected()){

                if(!channelIsActive[i]){//если установленно соединение, и канала не активен, значит релейка была повторно подключена и необходимо обнулить переменные
                    QMetaObject::invokeMethod(channelProcessing[i],"initialize",Qt::AutoConnection);
                    disconnectChannelMapper[0]->setMapping(radioRelay[0][i], i);
                    disconnectChannelMapper[1]->setMapping(radioRelay[1][i], i);
                    //
                    //fullPassTimer[i]->start();
                    //qDebug() << "fullPassTimer " << i << " " << fullPassTimer[i]->isActive() << fullPassTimer[i]->remainingTime();
                }
                channelIsActive[i] = true;
            }
            else{
                channelIsActive[i] = false;
                emit ("channelIsActive[] (test.cpp) i "  + QString::number(i) + QString::number(channelIsActive[i]));
                //fullPassTimer[i]->stop();
            }

            emit ("testIsActive (test.cpp) i "  + QString::number(i) + QString::number(testIsActive));

            if(testIsActive)
                emit channelActive(i,channelIsActive[i]);
        }
}

void Test::createConnect()
{
    disconnect(dlink3120[1],&Dlink3120new::portIsOn,this,&Test::createConnect); //отключаем коннект, после того как порт трафика генератора стартанул
    checkChannelIsActive();
 /*создаем коннекты для тех релеек, у которых channelIsActive равен false, то есть они либо были отключены от теста и снова подключены, либо это запуск теста*/
    for(int i = 0; i < channelIsActive.size(); i++){
        if(channelIsActive[i])
            connect(radioRelay[0][i],&RadioRelay::haveRRCInfo,channelProcessing[i],&ChannelProcessing::getRRCInfo);
            connect(radioRelay[1][i],&RadioRelay::haveRRCInfo,channelProcessing[i],&ChannelProcessing::getRRCInfo);            
        }
}

void Test::updateTimerLabel()
{
    emit updateOnePassTimerLabel(QTime::fromMSecsSinceStartOfDay(timeElapsed.elapsed()).toString("hh:mm:ss"));

    QStringList timerLabel;

    for(int i = 0; i < fullPassTimer.size();i++){
        int numOfday = fullPassTimer[i]->remainingTime() / oneDay;
        long int time = fullPassTimer[i]->remainingTime() - numOfday * oneDay;
        switch(numOfday){
            case 1: timerLabel.push_back("1 day " + QTime::fromMSecsSinceStartOfDay(time).toString("hh:mm:ss"));break;
            case 2:
            case 3:
            case 4: timerLabel.push_back(QString::number(numOfday) + " day " + QTime::fromMSecsSinceStartOfDay(time).toString("hh:mm:ss"));break;
            default: timerLabel.push_back(QString::number(numOfday) + " days " + QTime::fromMSecsSinceStartOfDay(time).toString("hh:mm:ss"));
        }
    }

    emit updateFullPassTimerLabel(timerLabel);
}

void Test::onePassTimeOut()
{
    emit ("onePassTimeOut (test.cpp)  ");
    emit turnOffDlink(trafficGeneratorPort);
    emit getStatDlink();//запрос инфы с длинка
    emit escalateAttValue();
    for (int i = 0;i < channelProcessing.size();i++){
        if(channelIsActive[i]){
            disconnect(radioRelay[0][i],&RadioRelay::haveRRCInfo,channelProcessing[i],&ChannelProcessing::getRRCInfo);//отключаеям сбор информации с релеек
            disconnect(radioRelay[1][i],&RadioRelay::haveRRCInfo,channelProcessing[i],&ChannelProcessing::getRRCInfo);//отключаеям сбор информации с релеек
        }
    }

    /*если вышло основное время*/
    bool resumeTest = false;
    for(int i = 0; i < fullPassTimer.size(); i++){
        emit ("fullPassTimer (test.cpp) " + QString::number(i) + QString::number(fullPassTimer[i]->isActive() ));
        if(!fullPassTimer[i]->isActive()){
            if(channelIsActive[i]){
                channelIsActive[i] = false;
                QMetaObject::invokeMethod(radioRelay[0][i],"stopHeat",Qt::AutoConnection);//выключение нагревателей
                QMetaObject::invokeMethod(radioRelay[1][i],"stopHeat",Qt::AutoConnection);//выключение нагревателей
                channelProcessing[i]->testFinished();
                emit channelActive(i,channelIsActive[i]);
                disconnectFromRRC(i);
            }            
            QMetaObject::invokeMethod(channelProcessing[i],"closeLogFile",Qt::AutoConnection);
        }
        else{
            resumeTest = true;
        }
    }

    emit ("resume test (test.cpp) " + QString::number(resumeTest ));

    if(resumeTest){
        connect(dlink3120[1],&Dlink3120new::portIsOn,this,&Test::createConnect);
        timeElapsed.restart();
    }
    else{
        testIsActive = false;
        emit testActive(testIsActive);
        onePassTimer->stop();
        updateTimer->stop();
        // write log
    }

    emit clearALLstat();
    emit turnOnDlink(trafficGeneratorPort);

}

void Test::dlinkSeparate(QVector<DlinkInfo> dlinkInfo)
{
    for(int i = 0; i < dlinkInfo.size(); i++)
        if(channelIsActive[i]){
            connect(this,&Test::separateDlinkInfoSignal,channelProcessing[i],&ChannelProcessing::getDlinkInfo);
            emit separateDlinkInfoSignal(dlinkInfo[i]);
            disconnect(this,&Test::separateDlinkInfoSignal,channelProcessing[i],&ChannelProcessing::getDlinkInfo);
        }
}

void Test::stopChannel(const int &numChannel)
{
    if(!channelIsActive[numChannel])
        return;

    emit ("STOP channel (test.cpp) " + QString::number(numChannel));

    /*убираем коннект, который при дисконнекте релейки снова вызовет эту функцию*/
    disconnectChannelMapper[0]->removeMappings(radioRelay[0][numChannel]);
    disconnectChannelMapper[1]->removeMappings(radioRelay[1][numChannel]);

    /*отсоединяем релейки*/
    disconnectFromRRC(numChannel);    
    //fullPassTimer[numChannel]->stop();
    disconnect(radioRelay[0][numChannel],&RadioRelay::haveRRCInfo,channelProcessing[numChannel],&ChannelProcessing::getRRCInfo);//конец сбора информации с релейки
    disconnect(radioRelay[1][numChannel],&RadioRelay::haveRRCInfo,channelProcessing[numChannel],&ChannelProcessing::getRRCInfo);//конец сбора информации с релейки

    QMetaObject::invokeMethod(channelProcessing[numChannel],"closeLogFile",Qt::AutoConnection);    

    /*сигнал выключающий кнопки*/
    emit channelActive(numChannel,channelIsActive[numChannel]);

    /*если нет активных тестов, то завершение теста*/

    for(int i = 0; i < channelIsActive.size(); i++)
        if(channelIsActive[i])
            return;

    /*если активных каналов нет*/
    emit ("NO ACTIVE CHANNEL (test.cpp) ");

    onePassTimer->stop();
    updateTimer->stop();

    testIsActive = false;
    emit testActive(testIsActive);
}

void Test::disconnectFromRRC(const int &numChannel)
{
    emit ("disconnectFromRRC (test.cpp) " + QString::number(numChannel));

    if(fullPassTimer[numChannel]->isActive())
        fullPassTimer[numChannel]->stop();

    channelIsActive[numChannel] = false;
    QMetaObject::invokeMethod(radioRelay[0][numChannel],"disconnectFromSnmp",Qt::AutoConnection);//отключение релейки
    QMetaObject::invokeMethod(radioRelay[1][numChannel],"disconnectFromSnmp",Qt::AutoConnection);//отключение релейки
}

void Test::setFullPassInterval(const int &msec)
{
    for(int i = 0; i < fullPassTimer.size(); i++)
        fullPassTimer[i]->setInterval(msec);
}

void Test::setOnePassInterval(const int &msec)
{
    onePassTimer->setInterval(msec);
}

Test::~Test()
{

}

