#ifndef CONFIG_H
#define CONFIG_H
#include <QSettings>
#include <QFileInfo>
#include <QDir>
#include <QtMath>
#include <QColor>
#include <QDebug>
struct Border{
    double yellowBorder;
    double redBorder;
};

class Config :public QObject
{
    Q_OBJECT
    QSettings *cnf;
    QString mibPath;
    Border perBorder;
    Border lostBorder;
    QString dlinkIp[2];
    QString generatorPort;
    QStringList listenPort;
    QColor notConnectedColor;
    QColor connectedColor;

public:
    Config(QObject *parent = 0);
    ~Config();
    void saveConfig();
    QString getMibPath();
    Border getPerBorder();
    Border getLostBorder();
    QString getDlinkIp0();
    QString getDlinkIp1();
    QString getGeneratorPort();
    QStringList getListenPort();
    QColor getNotConnectedColor();
    QColor getConnectedColor();

    void setMibPath(QString mibPath);
    void setDlinkIp0(const QString &dlinkIp);
    void setDlinkIp1(const QString &dlinkIp);
};

#endif // CONFIG_H
