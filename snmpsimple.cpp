#include "snmpsimple.h"
#include <QTime>
#include <QMessageBox>
#include <QFile>

SnmpSimple::SnmpSimple(QObject *parent) :
    QObject(parent),
    timeout(1000),
    interval(200),//100 - 1 секунда
    connectStat(false)
{

}

void SnmpSimple::start()
{
    initializeTarget = false;
    getTime = new QTimer(this);
    getTime->setInterval(timeout);
    Snmp::socket_startup();
    int status;
    snmp = new Snmp(status);
    if(status != SNMP_CLASS_SUCCESS)
        emit error("ERROR:" + (QString)snmp->error_msg(status));
    else{        
        connect(this,SIGNAL(startTimer()),getTime,SLOT(start()));
        connect(this,SIGNAL(stopTimer()),getTime,SLOT(stop()));        
    }

}

bool SnmpSimple::initializeSnmp()
{
    if(!address.valid()){
        emit error("ERROR: address not valid");
        initializeTarget = false;
        return false;
    }

    snmp_version version = version2c;
    int retries = 5;
    ctarget.set_address(address);
    ctarget.set_version(version);
    ctarget.set_retry(retries);
    ctarget.set_timeout(interval);
    ctarget.set_writecommunity(writeCommunityName.data());
    ctarget.set_readcommunity(readCommunityName.data());
    target = &ctarget;

    connect(getTime,SIGNAL(timeout()),this,SLOT(getStat()));
    initializeTarget = true;
    return true;
}

QString SnmpSimple::getSnmp(const QString &oidstr)
{

    if(!initializeTarget)
        return "";

    int status;
    Vb vb;
    Pdu pdu;
    Oid oid(oidstr.toLocal8Bit().data());
    vb.set_oid(oid);
    vb.set_syntax(sNMP_SYNTAX_UINT32);
    pdu += vb;
    QTime timer;
    timer.start();
    status = snmp->get(pdu, *target);
    if(status == SNMP_CLASS_SUCCESS){
        pdu.get_vb(vb,0);

        connectStat = true;
        //emit connected();

        if(vb.get_syntax() == sNMP_SYNTAX_CNTR64) {
            QString str = vb.get_printable_value();
            /*if(vb.get_asn1_length() > 23){
                qDebug() << "size" << str.size();
            }*/

            if(str.size() > 10){

                ULONGLONG num = str.toLongLong(0,16);
                return QString::number(num);
            }
            else{
                error("success" + QTime::fromMSecsSinceStartOfDay(timer.elapsed()).toString("hh:mm:ss.zzz"));
                return str;
            }
        }
        else{
            error("success" + QTime::fromMSecsSinceStartOfDay(timer.elapsed()).toString("hh:mm:ss.zzz"));
            return vb.get_printable_value();            
        }
    }
    else
    {
        if(status == SNMP_CLASS_TIMEOUT)
            error("ERROR: connection fail. Timeout" + QTime::fromMSecsSinceStartOfDay(timer.elapsed()).toString("hh:mm:ss.zzz"));
        else
            error("ERROR: snmp.get fail " + (QString)snmp->error_msg(status) + " " + QTime::fromMSecsSinceStartOfDay(timer.elapsed()).toString("hh:mm:ss.zzz"));
        QFile f("2.txt");
        f.open(QIODevice::WriteOnly | QIODevice::Text);
        QTextStream p(&f);
        p << (QString)snmp->error_msg(status);
        f.close();

        if(connectStat)
            disconnectFromSnmp();
    }
    return "";
}

QStringList SnmpSimple::getSnmp(QStringList oidstr)
{

    QStringList strList;
    if(!initializeTarget)
        return strList;
    int status;

    Vb vb;
    Pdu pdu;

    for(int i = 0;i < oidstr.size(); i++){
        Oid oid(oidstr[i].toLocal8Bit().data());

        vb.set_oid(oid);
        pdu += vb;
    }

    QTime timer;
    timer.start();

    status = snmp->get(pdu, *target);

    if(status == SNMP_CLASS_SUCCESS){
        connectStat = true;
        //emit connected();

        for(int i = 0; i < oidstr.size(); i++){
        pdu.get_vb(vb,i);

        if(vb.get_syntax() == sNMP_SYNTAX_CNTR64) {
            QString str = vb.get_printable_value();
            /*if(vb.get_asn1_length() > 23){
                qDebug() << "size" << str.size();
            }*/

            if(str.size() > 10){

                ULONGLONG num = str.toLongLong(0,16);
                strList << QString::number(num);
            }
            else
                strList << str;
        }
        else
            strList << vb.get_printable_value();
        }
        error("success"  + QTime::fromMSecsSinceStartOfDay(timer.elapsed()).toString("hh:mm:ss.zzz"));
        return strList;
    }
    else
    {
        if(status == SNMP_CLASS_TIMEOUT){
            error("ERROR: connection fail. Timeout " + QTime::fromMSecsSinceStartOfDay(timer.elapsed()).toString("hh:mm:ss.zzz"));
        }
        else{
            error("ERROR: snmp.get fail " + (QString)snmp->error_msg(status) + " "  + QTime::fromMSecsSinceStartOfDay(timer.elapsed()).toString("hh:mm:ss.zzz"));
        }
        if(connectStat){
            disconnectFromSnmp();
        }
    }
    return strList;
}

bool SnmpSimple::setSnmp(const QString& oidstr,const int& value)///
{
    if(!initializeTarget)
        return false;
    int status;
    Vb vb;
    Pdu pdu;
    //Oid oid(snmpCommands.value(command).data());
    Oid oid(oidstr.toLocal8Bit().data());
    vb.set_oid(oid);

    vb.set_syntax(sNMP_SYNTAX_INT);
    vb.set_value(value);
    pdu += vb;

    status = snmp->set(pdu,*target);

    if(status == SNMP_CLASS_TIMEOUT){
        error("ERROR: connection fail. Timeout");
        if(connectStat){
            disconnectFromSnmp();
        }
        return false;
    }
    if(status == SNMP_CLASS_SUCCESS){
            connectStat = true;
            //emit connected();
        return true;
    }
    else{
        if(connectStat){
            disconnectFromSnmp();
        }
        error("ERROR: snmp.set fail " + (QString)snmp->error_msg(status));
        return false;
    }
}

void SnmpSimple::disconnectFromSnmp()
{
    if(connectStat && initializeTarget){        
        disconnect(getTime,SIGNAL(timeout()),this,SLOT(getStat()));
        emit stopTimer();

        connectStat = false;

        initializeTarget = false;
        emit disconnected();
    }
}

SnmpSimple::~SnmpSimple()
{
    Snmp::socket_cleanup();
    delete snmp;
}
