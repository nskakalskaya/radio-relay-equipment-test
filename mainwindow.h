#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QFileDialog>
#include <QTabWidget>
#include <QMessageBox>
#include "widgets/connectinwindow.h"
#include "widgets/testwindow.h"
#include "widgets/parameterswindow.h"
#include "config.h"
#include <sstream>
#include <windows.h>
#include <QString>
#include <QByteArray>
#include <QMap>
#include "SLABCP2110.h"



using namespace std;

typedef quint16 OIDTYPE;
typedef list<OIDTYPE> OIDARR;

struct OidIdentifier{
    OIDARR oid;
    string name;
};



template <typename T>
T stringToNumber( const string &text, bool &ok,const T defValue = T() )
{
    stringstream ss(text);

    T result;
    ss >> result;
    if(ss.fail()){
        ok = false;
        return defValue;
    }

    ok = true;
    return result;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT
    const WORD VID = 0x10C4;
    const WORD PID = 0xEA80;
    HID_UART_DEVICE devHandle;
    list <OidIdentifier> oidList;
    QVector<QString>OidColumn;

    Config config;
    const unsigned short numOfDlink;//количество длинков

    ConnectinWindow *connectinWindow;
    TestWindow *testWindow;
    ParametersWindow *paramsWindow;

    QVector <RadioRelay *> radioRelay[2];
    QVector <QThread *> radioThread[2];
    QVector <Dlink3120new * >dlink3120;

    QThread dlinkThread[2];
    QVector <AttenuatorPPC *> attenuatorPPC;

    QString pathToMIBFile;

    QHash <QString,QString> snmpCommands; //для хранения oid команд, первое значение имя команды(как и в остальных мапах)
    QHash <QString,QString> commandDescription; //расшифровка команд

    bool loadMIBFile();

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
};

#endif // MAINWINDOW_H
