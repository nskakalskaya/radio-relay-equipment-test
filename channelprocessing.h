#ifndef CHANNELPROCESSING_H
#define CHANNELPROCESSING_H

#include <QObject>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QTimer>
#include <JlCompress.h>

#include "RRC/radiorelaynew.h"
#include "dlink/dlink3120new.h"
#include "config.h"
/*класс по обработки информации с устройств
    тут происходит формирование отчета,
    подсчет перов и оценка цвета*/

class ChannelProcessing : public QObject
{
    Q_OBJECT

    /*структура файла логов для релейка*/
    struct LogStruct{
        RRCInfo rrc;
//        QString totalRRCRXPacket;
//        QString totalRRCTXPacket;
//        QString deltaError;
//        QString PER;
    };
    struct TimeDuration{
        QString start;
        QString stop;
    } timeDuration;

/*сумма пакетов и байт, и ошибок, после каждого прохода*/
    qint64 totalRRCRXPacket[2];
    qint64 totalRRCTXPacket[2];
    qint64 totalDlinkRXPacketGood[2];
    qint64 totalDlinkTXPacket[2];
    qint64 totalDlinkErrors[2];
    quint64 DlinkErrors[2];
    qint64 RXPacketsGOOD[2];

    unsigned long startError[2];

    RRCInfo rrcInfo[2];////del относится к log
    DlinkInfo dlinkInfo[2];
    bool receiveRRCInfo[2];
    bool receiveDlinkInfo[2];
    bool isNeedToCompress;//надо ли сжимать файлы
    Border perBorder, lostBorder;
    double logPER[2];
    double logError[2];


    QFile *logFile;

    QString dirName;
    QString fullPathName;
    QString tableHeaderHTML[2];
    QString dlinkTableHeaderHTML;
    QString resultTableHeaderHTML;
    QString model[2];
    QString serialNumber[2];
    QString colorChannel;    
    quint16 numStatusRRC[2],numStatusDlink[2],numStatusLostDlink[2];//индивидуальный цифровой статус для релейки, для длинка
    QTimer *shutDownTimer;//этот таймер запускается, когда цвет канала становится желтым или красным
    bool testIsFinished;

    QVector <LogStruct> rrcLog[2]; // здесь хранится информация о релейки за последний проход
QString serialname = " ";

    void writeRRCInfo();
    void writeDlinkInfo();
    quint16 checkPER(double &PER);//проверяется в каком диапозоне лежат перы, возвращает цифровой статус канала
    quint16 checkLostStatus(const double &lost);
    //void checkLost();
    void checkChannelColorStatus();//оценка индивидуальных цветов каждого устройства и вывод общего цвета канала
    bool openLogFile();  

public:
    explicit ChannelProcessing(const unsigned int& numChannel,
                               const unsigned int& numPort,
                               Config &cnf,
                               QObject *parent = 0);///
    void setModel(const int &numRadio, const QString &model);
    void setSerialNumber(const int &numRadio, const QString &serialNumber);

    void testFinished();
    void setComress(bool state);
    ~ChannelProcessing();

signals:
    void colorStatus(QColor color);
    void resultComlpete();
    void rrcPERSignal(const int& numRadio,const double& PER);
    void dlinkPERSignal(const int& numDlink,const double& PER);
    void totalDlinkPERSignal(const int& numDLink,const double& PER);
    void lostDlinkSignal(const int& numDLink,const double& lost);//отправляем число лостов
    void stopChannel();
    void setShutDownTime(const QString& str);
    void toLog(QString str);

public slots:
    void initialize();
    void start();
    void getRRCInfo(RRCInfo rrcInfo);
    void getDlinkInfo(DlinkInfo dlinkInfo);
    void closeFile();
    void closeLogFile();
    void getModelName(QString modelname);
   // void creatingHTMLReport();
};

#endif // CHANNELPROCESSING_H
